Proposed agenda for Gaia-X OSS community August 24, 2023
Please add any wishes for the agenda here!

    Competition & Antitrust guidelines (1 min, Kai)

    Acceptance of last week meeting notes and today's agenda (1 min, Kai)

    Introduction of new participants and regular contributors (1-10 min, Kai & newcomers)

    Interesting sessions, events and news (5-10 min, all)

    Gaia-X Digital Clearing Houses (GXDCH) and Endpoints for Compliance

    Updates from other Lighthouses / Projects / Gaia-X Lab/ Developers (all) 

    AOB (all)


Meeting notes Gaia-X OSS community August 24, 2023
These notes will be removed every Tuesday EOB before the next call and then be shared via the mailing list. Please review/make your changes until that day. Thank you!
Agenda

    Competition & Antitrust guidelines (1 min, Kai)

    Acceptance of last week meeting notes and today's agenda (1 min, Kai)

    Introduction of new participants and regular contributors (1-10 min, Kai & newcomers)

    Interesting sessions, events and news (5-10 min, all)

    Updates from other lighthouses / projects / Gaia-X Lab/ developers (all) 

    AOB (all)


 Focus of the weekly

    Updates from the community


General Notes

    Participants in the call: 37

    Acceptance of Agenda: yes

    Acceptance of last week's minutes: yes

    Link to last week's minutes:https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes_weekly_meeting/Minutes_OSS-Community-Call_2023-08-17.md

    Link to all minutes: https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes_weekly_meeting


Introduction of new participants and regular contributors

    Ali Hariri, Sovereignty, dataspace & GaiaX @ Huawei


Useful resources for newcomers:

    Introduction to our Community: https://gitlab.com/gaia-x/gaia-x-community/open-source-community

    Link to our mailing list: https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu

    The Gaia-X Framework: https://docs.gaia-x.eu/framework/

    Gaia-X Publications : https://gaia-x.eu/mediatech/publications/

    Subscribe to Gaia-X Tech Newsletter: https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/

    Gaia-X Wizard https://wizard.lab.gaia-x.eu/

    Gaia-X Compliance Service Swagger UI https://compliance.lab.gaia-x.eu/v1/docs/

    Gaia-X Registry Swagger UI: https://registry.lab.gaia-x.eu/v1/docs/#/

    Gaia-X Registry: https://registry.lab.gaia-x.eu/

    Gaia-X Registration Number Service: https://registrationnumber.notary.gaia-x.eu/v1/docs/

    OSS Group Calendar: https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar

    Gaia-X Members Platform: https://membersplatform.gaia-x.eu/


Interesting sessions, events and news
Gaia-X Hub Austria / Forum Alpbach by AIT 

    August 25-27

    https://www.gaia-x.at/efa23/


Gaia-X Hub Austria Gaia-X Hub Meetup

    August 28, 2023

    https://ai5production.at/event/datenraeume-konkrete-anwendungen-fuer-die-produktion-der-zukunft/

    https://www.meetup.com/gaia-x-austria/events/295149661/


GXFS CONNECT 2023 (English speaking event)

    September 5-6, 2023, Berlin

    https://www.gxfs.eu/gxfs-connect-2023/

    Max. PAX 250

    Agenda in the making

    English-speaking event


Next GXFS Workshops (on site - language English)

    5 & 6 September, Berlin, parallel to GXFS Connect 2023, also in Villa Elisabeth

    25. Oktober, Reutlingen 

    12-13 December, Köln 


IAA Mobility

    September 6-8 in Munich, Germany

    Gaia-X 4 Future Mobility Demonstrator incl. SSI and network usage available with vehicles and smart infrastructure

    Registration for test drives https://mobix.ai/iaa-moveid/


Gaia-X Roadshow 

    10.10.2023 18:00 - 23:00 Uhr, Wolfsburg

    Registration: https://www.eco.de/event/gaia-x-roadshow-wolfsburg 


Best of Blockchain

    October 5-7, Berlin

    https://bestofblockchain.io/

    Panel on European data economy and Gaia-X spearheading Web3 and Web2 integration for businesses


SODA DATA Vision 2023 (in parallel with Open Source Summit Europe | Linux Foundation Events 19-21 September)

    September 18, Bilbao

    Call for proposals - until July 31st. Hybrid.

    https://www.sodafoundation.io/events/sodadatavision2023/

    Topics (among others):

    Data Catalog for Files and Object

    Cloud Native Data Management

    Hybrid Multi-Cloud Data Lifecycle & Protection


EuProGigant Open House Day 

    EuProGigant ecosystem of five projects, meeting in Berlin

    October 10-11, 2023

    Registration open, 250 PAX, physical participation only

    https://euprogigant-openhouseday2023.b2match.io/


Future Congress Digital 

    October 11, Wolfsburg (Germany)

    Gaia-X and Digital Mobility Ecosystems

    https://www.futurecongress.digital/anmeldung


EclipseCon

    October 16 - 19, Ludwigsburg (Germany)

    https://www.eclipsecon.org/2023

    Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects


Gaia-X Summit 2023

    November 9-10, 2023

    Alicante, Spain

    https://gaia-x.eu/summit-2023/


ALASCA Tech-Talk August

    31 August, 2-3 pm (Online)

    Topic: Using Confidential Computing for Protecting Data, Code, and Secrets of Applications

    Speaker: Christof Fetzer (Co-Founder of Scontain)

    Participation open to everyone


Tech deep dive

    September 12th, 12h-13h CEST

    Online

    Topic: Quick tour of the implementation of the TF 22.10

    Speaker: Ewann (Gaia-X AISBL CTO Team)


Gaia-X Digital Clearing Houses (GXDCH)

    Aruba.it is the only available Gaia-X Digital Clearing House today

    Additional instance being maintained by Gaia-X AISBL

    Traffic is channeled through the loadbalancer and Aruba endpoints can be used as backup

    Overall description


https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/loadbalancer/haproxy.cfg?ref_type=heads
 
GXDCH registry v1: https://registry.gaia-x.eu/v1/docs
GXDCH compliance service v1: https://compliance.gaia-x.eu/v1/docs
GXDCH notary v1: https://registrationnumber.notary.gaia-x.eu/v1/docs
 
Aruba.it Registry v1: https://gx-registry.aruba.it/v1/docs/
Aruba.it Notary v1: https://gx-notary.aruba.it/v1/docs/
Aruba.it Compliance v1: https://gx-compliance.aruba.it/v1/docs/
 
Lab registry v1: https://registry.lab.gaia-x.eu/v1/docs
Lab registry main: https://registry.lab.gaia-x.eu/main/docs
Lab registry dev: https://registry.lab.gaia-x.eu/development/docs
 
Lab compliance v1: https://compliance.lab.gaia-x.eu/v1/docs
Lab compliance main: https://compliance.lab.gaia-x.eu/main/docs
Lab compliance dev: https://compliance.lab.gaia-x.eu/development/docs
 
Lab notary v1: https://registrationnumber.notary.lab.gaia-x.eu/v1/docs
Lab notary main: https://registrationnumber.notary.lab.gaia-x.eu/main/docs
Lab notary dev: https://registrationnumber.notary.lab.gaia-x.eu/development/docs
 
Gaia-X Wizard connected GXDCH v1/main/dev : https://wizard.lab.gaia-x.eu/  
Gaia-X Wizard development version: https://wizard.lab.gaia-x.eu/development
 
GXDCH repository : https://gitlab.com/gaia-x/lab/gxdch

Further important points regarding Gaia-X compliance from the PRC: https://gaia-x.gitlab.io/policy-rules-committee/label-document/data_exchange/

Updates from other lighthouses / projects / developers / lab?

    Release and deployment done:  compliance 1.6.0, registry 1.7.0, notary 1.5.0, CES 1.0.0, wizard 1.6.0

    Changelog in each project, in slack also.

    Gaia-X Wizard, check out the development version before it becomes main: https://wizard.lab.gaia-x.eu/development

    Pontus-X and Eclipse XFSC integration in development for ecosystem interoperability and identity ecosystem integration. Integration results to be shared with other ecosystem projects.

    Landscape map is under development, sharing preview after the next release

    If you want to share your endpoints and available instances and they are not public knowledge yet, please add them here


 
AOB

    GXFS-DE Moves to Eclipse Foundation under the name Eclipse XFSC (Cross Federation Services Components)  PR: https://www.gxfs.eu/move-to-eclipse-foundation/ 

    https://projects.eclipse.org/projects/technology.xfsc 

    Are the SiMPL tender results already announced?

    Not the case, no information on that is publicly available

    Will SiMPL lean on Gaia-X

    Strong indications for that in the public tender documents and expected by many Gaia-X community members




