# Meeting notes Gaia-X OSS community January 26, 2023

## Focus of the weekly

   * Presentation on Eclipse
   * Hackathon No. 6

## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Presentation on Eclipse w. Florent Zara \& Boris Baldassari, Eclipse Foundation

6. Tech event \& Hackathon #6

7. AOB

## General Notes

**Participants in the call:** 41

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-01-19.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-01-19.md)

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)

### Introduction of new participants and regular contributors

   * Mehrdad Salari
   * Paulo Cabrita
   * Gaël Blondelle (Eclipse Foundation), Chief Membership Officer of the Eclipse Foundation

**Useful resources for newcomers:**

   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/) \& [https://docs.gaia-x.eu/framework/beta/](https://docs.gaia-x.eu/framework/beta/)
   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)

### Interesting sessions, events and news


   * **ALASCA Tech-Talk #1: Yaook: Using Kubernetes for deploying OpenStack, a non-cloud-native application**
       * January 26 (2 - 3 pm, online)
       * [https://alasca.cloud/alasca-tech-talk-1/](https://alasca.cloud/alasca-tech-talk-1/)
       * Join here: [https://us02web.zoom.us/j/89142262107?pwd=d0VXa05TQXpEa3crSFhHSk04ZWt0Zz09](https://us02web.zoom.us/j/89142262107?pwd=d0VXa05TQXpEa3crSFhHSk04ZWt0Zz09)
       * Organizer: **ALASCA - Verband für betriebsfähige, offene Cloud-Infrastrukturen e.V.**


   * **Gaia-X Hub Germany - Participant Onboarding and Self-Description Workshop**
       * February 3, 09:00 to 12:00 CET
       * Organized by acatech and deltaDAO
       * Will be held in English due to the huge interest from the international community
       * Creation of Gaia-X compliant participant Self-Descriptions to reach the 50% adoption goal of 2023 and to prepare projects for the Gaia-X Clearing Houses and work with Self-Descriptions
       * Includes Onboarding for the Gaia-X Web3 ecosystem


   * **FOSSDEM**
       * 4-5 February, Brussels
       * [https://fosdem.org/2023/schedule/](https://fosdem.org/2023/schedule/)
       * SCS devroom: [https://fosdem.org/2023/schedule/track/sovereign\_cloud/](https://fosdem.org/2023/schedule/track/sovereign\_cloud/)


   * **Manufacturing Data Space Event, virtual**
       * Register here: [https://share-eu1.hsforms.com/1Jx3F3Pt\_TYKnHNEFamUiTQfjelv](https://share-eu1.hsforms.com/1Jx3F3Pt\_TYKnHNEFamUiTQfjelv)
       * Information: [https://gaia-x.eu/event/gaia-x-manufacturing-data-space-event/](https://gaia-x.eu/event/gaia-x-manufacturing-data-space-event/)


   * **Gaia-X Roadshow Hamburg**
       * February 9, 2023, organized by eco
       * [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)
       * [https://www.eco.de/event/gaia-x-roadshow-powered-by-eco-hamburg/#tickets](https://www.eco.de/event/gaia-x-roadshow-powered-by-eco-hamburg/#tickets)
       * Restaurant Vlet in der Speicherstadt


   * **FOSS Backstage 23**
       * March 14-15, Berlin (and online)
       * Theme: *Community, Management \& Compliance in Open Source Development*
       * [https://23.foss-backstage.de/](https://23.foss-backstage.de/)
       * Eduard \& Ariane will be there to present **Community2**
           * Community2 is our new community for Open Source Community Managers
           * More information: [https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop](https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop)


   * **Market-X, Vienna, Gaia-X Market Adoption Event**
       * March 14-15, Vienna, Aula der Wissenschaften, [https://www.aula-wien.at/](https://www.aula-wien.at/) 
       * Updated website: [https://gaia-x.eu/market-x/](https://gaia-x.eu/market-x/)
       * Market-X is a great opportunity to present projects, find business matches, connect with our lighthouse projects and other initiatives, start building cross-industry collaborations, learn about Gaia-X value chains on different levels and become part of Gaia-X market adoption community.
       * The event will feature a large exhibition where you can network and connect with the community, as well as different booth options to showcase hubs, projects, verticals, and get visibility within the Gaia-X community. Learn about the booth options at  [https://gaia-x.eu/market-x/](https://gaia-x.eu/market-x/)


   * **Data Spaces Discovery Days Netherlands - Business value of sovereign data sharing**
       * March 21–23
       * Pre-registration link: [https://internationaldataspaces.org/data-spaces-discovery-days-netherlands](https://internationaldataspaces.org/data-spaces-discovery-days-netherlands) 
       * It seems the name has been changed and there's a new link (Not sure if it's the same event, but the days are the same): [https://internationaldataspaces.org/events/data-spaces-dicovery-days-the-hague-business-value-of-sovereign-data-sharing/](https://internationaldataspaces.org/events/data-spaces-dicovery-days-the-hague-business-value-of-sovereign-data-sharing/)


   * **Hannover Messe, Hannover, Germany**
       * April 17-21, 2023 (five days is new in 23)
       * [https://www.hannovermesse.de/en/](https://www.hannovermesse.de/en/)
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de
       * Gaia-X Area


   * **Cloud Expo Europe, Frankfurt, Germany**
       * May 10-11, 2023
       * [https://www.cloudexpoeurope.de/themes2023](https://www.cloudexpoeurope.de/themes2023)
       * Gaia-X at the heart of the Expo, Gaia-X Arena 
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de


   * **MyData conference**
       * May 31 - June 01, Helsinki
       * [https://www.mydata.org/event/mydata-2023/](https://www.mydata.org/event/mydata-2023/)


   * **New event series: GXFS Tech Workshops**
       * There will be a series of GXFS Tech Workshops hosted over the year
           * Two days hands-on workshops, organized by eco
           * Working on several use cases and concrete implementations
       * Around March or April, most likely hosted in Cologne
           * Livestream also planned for passive participation

### Introduction and AMA on Eclipse

   * Eclipse Foundation AISBL moved to Europe, [https://www.eclipse.org/europe/](https://www.eclipse.org/europe/)
   * Tractus-X currently most active project in Eclipse
   * Get started at [http://eclipse.org/projects/handbook/](http://eclipse.org/projects/handbook/)
   * Slides are available at [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-01-26\_Eclipse-Fnd-slides.pdf](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-01-26\_Eclipse-Fnd-slides.pdf)


Questions: 

   * **Is there an imposed OSS license?** Focus on EPL V2.0, Apache V2.0, MIT and BSD. block anything under AGPL as of now. You can have other licences and discuss them with the team.
   * **Is Eclipse Foundation hosting only Java projects?** No.
   * **Are there costs associated to working groups?** Everybody can participate in projects. Project Submitter needs to be part of the Foundation. Working groups are for members, starting at 300k/annually. 
   * **What's required to start a new working group?**
   * **Can you share the slides with us after the talk?** Yes.
   * **How to find a mentor?** EMO will find a mentor.
   * **How does the "business model" work with regard to the availability of "the commons", such as mentors, support, etc.?** See above.
   * **How to move already existing projects under the hood of Eclipse?** Answered and there is a good handbook on the topic. In short: it's basically the same, the definition of the project will be easier, and we'll snapshot your existing repository for the IP review of the initial contribution.


### Tech event \& Hackathon No. 6

   * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home)
   * **Location and date:** Bilbao, first week of May
   * **Call for contributions:** If you have anything in mind for the Hackathon, please add to the proposals page (or align on suggestions that have already been added, preferably via the mailing list) [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home)
       * **Deadline for proposals:** roughly end of March
   * **Call for organizers:** If you would like to help organizing the Hackathon, please reach out!


### AOB

   * [nothing]





