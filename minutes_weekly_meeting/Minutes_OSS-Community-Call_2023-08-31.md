# Meeting notes Gaia-X OSS community August 31, 2023

## Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Gaia-X Compliance and Trust Framework 22.10 (new v1 and main version)
   6. Gaia-X Digital Clearing Houses (GXDCH)
   7. Updates from other lighthouses / projects / Gaia-X Lab/ developers 
   8. AOB

##  Focus of the weekly

   * Gaia-X Compliance and Trust Framework 22.10


## General Notes

   * **Participants in the call:** 35
   * **Acceptance of Agenda:** yes
   * **Acceptance of last week's minutes:** yes
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-08-24.md](**https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-08-24.md**)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * No new participants in the call.


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**ALASCA Tech-Talk August**

   * 31 August, 2-3 pm (Online)
   * Topic: Using Confidential Computing for Protecting Data, Code, and Secrets of Applications
   * Speaker: Christof Fetzer (Co-Founder of Scontain)
   * Participation open to everyone


**GXFS CONNECT 2023 (English speaking event)**

   * September 5-6, 2023, Berlin
   * [https://www.gxfs.eu/gxfs-connect-2023/](https://www.gxfs.eu/gxfs-connect-2023/)
   * Max. PAX 250
   * Agenda in the making
   * English-speaking event


**Next GXFS Workshops (on site - language English)**

   * 5 \& 6 September, Berlin, parallel to GXFS Connect 2023, also in Villa Elisabeth
   * 25. Oktober, Reutlingen 
   * 12-13 December, Köln 


**Tech deep dive**

   * September 12th, 12h-13h CEST
   * Online
   * Topic: Quick tour of the implementation of the TF 22.10
   * Speaker: Ewann (Gaia-X AISBL CTO Team)


**IAA Mobility**

   * September 6-8 in Munich, Germany
   * Gaia-X 4 Future Mobility Demonstrator incl. SSI and network usage available with vehicles and smart infrastructure
   * Registration for test drives [https://mobix.ai/iaa-moveid/](https://mobix.ai/iaa-moveid/)


**Gaia-X Roadshow** 

   * 10.10.2023 18:00 - 23:00 Uhr, Wolfsburg
   * Registration: [https://www.eco.de/event/gaia-x-roadshow-wolfsburg](https://www.eco.de/event/gaia-x-roadshow-wolfsburg) 


**SODA DATA Vision 2023 (in parallel with **Open Source Summit Europe | Linux Foundation Events 19-21 September)

   * September 18, Bilbao
   * Call for proposals - until July 31st. Hybrid.
   * [https://www.sodafoundation.io/events/sodadatavision2023/](https://www.sodafoundation.io/events/sodadatavision2023/)
   * Topics (among others):
       * Data Catalog for Files and Object
       * Cloud Native Data Management
       * Hybrid Multi-Cloud Data Lifecycle \& Protection


**EuProGigant Open House Day** 

   * EuProGigant ecosystem of five projects, meeting in Berlin
   * October 10-11, 2023
   * Registration open, 250 PAX, physical participation only
   * [https://euprogigant-openhouseday2023.b2match.io/](https://euprogigant-openhouseday2023.b2match.io/)


**Future Congress Digital** 

   * October 11, Wolfsburg (Germany)
   * Gaia-X and Digital Mobility Ecosystems
   * [https://www.futurecongress.digital/anmeldung](https://www.futurecongress.digital/anmeldung)


**EclipseCon**

   * October 16 - 19, Ludwigsburg (Germany)
   * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
   * Workshop "Federation Services @ Eclipse" during the community day on Monday, October 16th 
       * [https://www.eclipsecon.org/2023/community-day](https://www.eclipsecon.org/2023/community-day)
   * Dedicated workshop "Federation Services" at the first day of the week. 


**Gaia-X Summit 2023**

   * November 9-10, 2023
   * Alicante, Spain
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


### Gaia-X Compliance and Wizard, new v1 and main version

New compliance version according to the Gaia-X Trust Framework 22.10 has been released to catch up with the current specifications of the Trust Framework.

Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)

Trust Framework: [https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)

### Gaia-X Digital Clearing Houses (GXDCH)

   * Aruba.it is the only available Gaia-X Digital Clearing House today
   * Additional instance being maintained by Gaia-X AISBL
   * Traffic is channeled through the loadbalancer and Aruba endpoints can be used as backup
   * Arsys also deployed instances of the registry, compliance and notarization services that can be used.
   * Overall description


[https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/loadbalancer/haproxy.cfg?ref\_type=heads]([]https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/loadbalancer/haproxy.cfg?ref\_type=heads[])


GXDCH registry v1: [https://registry.gaia-x.eu/v1/docs]([]https://registry.gaia-x.eu/v1/docs[])

GXDCH compliance service v1: [https://compliance.gaia-x.eu/v1/docs]([]https://compliance.gaia-x.eu/v1/docs[])

GXDCH notary v1: [https://registrationnumber.notary.gaia-x.eu/v1/docs]([]https://registrationnumber.notary.gaia-x.eu/v1/docs[])

 

Aruba.it Registry v1: [https://gx-registry.aruba.it/v1/docs/]([]https://gx-registry.aruba.it/v1/docs/[])

Aruba.it Notary v1: [https://gx-notary.aruba.it/v1/docs/]([]https://gx-notary.aruba.it/v1/docs/[])

Aruba.it Compliance v1: [https://gx-compliance.aruba.it/v1/docs/]([]https://gx-compliance.aruba.it/v1/docs/[])

 

Lab registry v1: [https://registry.lab.gaia-x.eu/v1/docs]([]https://registry.lab.gaia-x.eu/v1/docs[])

Lab registry main: [https://registry.lab.gaia-x.eu/main/docs]([]https://registry.lab.gaia-x.eu/main/docs[])

Lab registry dev: [https://registry.lab.gaia-x.eu/development/docs]([]https://registry.lab.gaia-x.eu/development/docs[])

 

Lab compliance v1: [https://compliance.lab.gaia-x.eu/v1/docs]([]https://compliance.lab.gaia-x.eu/v1/docs[])

Lab compliance main: [https://compliance.lab.gaia-x.eu/main/docs]([]https://compliance.lab.gaia-x.eu/main/docs[])

Lab compliance dev: [https://compliance.lab.gaia-x.eu/development/docs]([]https://compliance.lab.gaia-x.eu/development/docs[])

 

Lab notary v1: [https://registrationnumber.notary.lab.gaia-x.eu/v1/docs]([]https://registrationnumber.notary.lab.gaia-x.eu/v1/docs[])

Lab notary main: [https://registrationnumber.notary.lab.gaia-x.eu/main/docs]([]https://registrationnumber.notary.lab.gaia-x.eu/main/docs[])

Lab notary dev: [https://registrationnumber.notary.lab.gaia-x.eu/development/docs]([]https://registrationnumber.notary.lab.gaia-x.eu/development/docs[])

 

Arsys registry v1: [https://registry.arlabdevelopments.com/v1/docs/](https://registry.arlabdevelopments.com/v1/docs/)

Arsys registry main: [https://registry.arlabdevelopments.com/main/docs/](https://registry.arlabdevelopments.com/main/docs/)

Arsys registry dev: [https://registry.arlabdevelopments.com/development/docs/](https://registry.arlabdevelopments.com/development/docs/)

 

Arsys compliance v1: [https://compliance.arlabdevelopments.com/v1/docs/](https://compliance.arlabdevelopments.com/v1/docs/)

Arsys compliance main: [https://compliance.arlabdevelopments.com/main/docs/](https://compliance.arlabdevelopments.com/main/docs/)

Arsys compliance dev: [https://compliance.arlabdevelopments.com/development/docs/](https://compliance.arlabdevelopments.com/development/docs/)

 

Arsys notary v1: [https://registrationnumber.notary.arlabdevelopments.com/v1/docs/](https://registrationnumber.notary.arlabdevelopments.com/v1/docs/)

Arsys notary main: [https://registrationnumber.notary.arlabdevelopments.com/main/docs/](https://registrationnumber.notary.arlabdevelopments.com/main/docs/)

Arsys notary dev: [https://registrationnumber.notary.arlabdevelopments.com/development/docs/](https://registrationnumber.notary.arlabdevelopments.com/development/docs/)

 

Gaia-X Wizard connected GXDCH v1/main/dev : [https://wizard.lab.gaia-x.eu/]([]https://wizard.lab.gaia-x.eu/[])  

Gaia-X Wizard development version: [https://wizard.lab.gaia-x.eu/development]([]https://wizard.lab.gaia-x.eu/development[])

 

GXDCH repository : [https://gitlab.com/gaia-x/lab/gxdch]([]https://gitlab.com/gaia-x/lab/gxdch[])



Further important points regarding Gaia-X compliance from the PRC: [https://gaia-x.gitlab.io/policy-rules-committee/label-document/data\_exchange/](https://gaia-x.gitlab.io/policy-rules-committee/label-document/data\_exchange/)



### Updates from other lighthouses / projects / developers / lab?

   * Release and deployment done:  compliance **1.6.0**, registry **1.7.0**, notary** 1.5.0**, CES **1.0.0**, wizard **1.6.0**
       * Changelog in each project, in slack also.
       * Small hiccups being fixed/managed
   * Pontus-X and Eclipse XFSC integration in development for ecosystem interoperability and identity ecosystem integration. Integration results to be shared with other ecosystem projects.
   * Remote Attestation being worked on to scale the compliance-related infrastructure. Contributions are highly welcome
       * [https://keylime.dev/](https://keylime.dev/) (check it out)
 

### AOB

   * GXFS-DE Moves to Eclipse Foundation under the name Eclipse XFSC (Cross Federation Services Components)  PR: [https://www.gxfs.eu/move-to-eclipse-foundation/](https://www.gxfs.eu/move-to-eclipse-foundation/) 
       * [https://projects.eclipse.org/projects/technology.xfsc](https://projects.eclipse.org/projects/technology.xfsc) 
   * Community input/support for submitting a proposal for Next Generation Internet [https://trustchain.ngi.eu/open-call-2/](https://trustchain.ngi.eu/open-call-2/) (deadline 'sportlich' **20 September**). Idea: further develop the Unigrid Decentralized Internet ([https://unigrid.org)](https://unigrid.org)) while ensuring maximum interoperability / standardization, e.g. in line with Gaia-X, maybe EBSI, etc. (René Krikke, engage@mrprobot.com - not sure if I can make the meeting).

























