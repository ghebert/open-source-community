# Meeting notes Gaia-X OSS community June 21, 2023

## Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Gaia-X Academy
   6. Pontus-X Nautilus Release and Demo
   7. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   8. AOB

##  Focus of the weekly

   * Updates from the community
   * Gaia-X Academy
   * Nautlius Release and Demo

## General Notes

   * **Participants in the call:** 31
   * **Acceptance of Agenda:** Accepted by the audience.
   * **Acceptance of last week's minutes:** Accepted by the audience.
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-06-15.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-06-15.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Pietro Bartoccioni,  Aruba-IT, DCH provider, member
   * Frederik Tengg, Gaia-X AISBL, Gaia-X Academy


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Compliance Service Swagger UI [https://compliance.lab.gaia-x.eu/v1/docs/](https://compliance.lab.gaia-x.eu/v1/docs/)
   * Gaia-X Registry Swagger UI: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Gaia-X Registry: [https://registry.lab.gaia-x.eu/](https://registry.lab.gaia-x.eu/)
   * Gaia-X Registration Number Service: [https://registrationnumber.notary.gaia-x.eu/v1/docs/](https://registrationnumber.notary.gaia-x.eu/v1/docs/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

   * **Interesting reads**
       * Some reading about eco systems and Gaia-X : [https://www.dotmagazine.online/](https://www.dotmagazine.online/)
           * [https://www.dotmagazine.online/issues/digital-business-models-ecosystems/trust-and-collaboration-the-enablers-of-digital-business-models-and-ecosystems](https://www.dotmagazine.online/issues/digital-business-models-ecosystems/trust-and-collaboration-the-enablers-of-digital-business-models-and-ecosystems) (by Emma \& Lauresha)


   * **GXFS Tech Workshop**
       * 21 \& 22 June in Braunschweig - Physical only
       * Register here: [https://www.eco.de/event/gxfs-tech-workshop-2/](https://www.eco.de/event/gxfs-tech-workshop-2/) 
       * The workshop will be comprised of introductory sessions and exercises on day one focusing on the GXFS work packages identity and trust as well as catalogue and asset descriptions. We will also hear the latest update on EDC components. On day two, we will have hacking sessions focused on exploring how GXFS and EDC components can be used.

   * **CloudLand**
       * Jun 20 - 23, 2023 in Phantasialand Brühl
       * [https://www.cloudland.org/](https://www.cloudland.org/)


   * **Eclipse XFSC Into webinar**
       * Jun 26, 2023 3:00 PM - 4:30 PM CEST
       * [https://www.linkedin.com/posts/emma-wehrwein-eco\_gxfs-xfsc-xfsc-activity-7069670712877150210-5VA3?utm\_source=share\&utm\_medium=member\_desktop](https://www.linkedin.com/posts/emma-wehrwein-eco\_gxfs-xfsc-xfsc-activity-7069670712877150210-5VA3?utm\_source=share\&utm\_medium=member\_desktop) 

   * **New GXFS Whitepaper:** [https://www.gxfs.eu/kritis-whitepaper/](https://www.gxfs.eu/kritis-whitepaper/)
       * Which rules operators of critical infrastructures have to follow in the context of Gaia-X, is discussed in the new Gaia-X Federation Services (GXFS-DE) whitepaper “Information Security of Critical Infrastructures – Law and Regulation for Gaia-X and the Gaia-X Federation Services”.


   * **5th HACKATHONAMRING** - Physical only
       * Date and time: June 23 · 4pm - June 25 · 4pm CEST
       * [https://www.eventbrite.de/e/5th-hackathonamring-tickets-190977106667?aff=](https://www.eventbrite.de/e/5th-hackathonamring-tickets-190977106667?aff=)   


   * **Gaia-X Public Data Space Event**
       * June 27, 2023
       * [https://gaia-x.eu/event/public-sector-data-space-event/](https://gaia-x.eu/event/public-sector-data-space-event/)


   * **SODA DATA Vision 2023**
       * September 18, Bilbao
       * Topics:
           * Data Catalog for Files and Object
           * Cloud Native Data Management
           * Hybrid Multi-Cloud Data Lifecycle \& Protection
           * Ransomware
           * Cyberstorage, Immutable Storage
           * Data Isolation
           * Data Governance - PII, GDPR, CCPA, HIPPA
           * Storage Observability
           * Emerging Storage Technologies


   * **EclipseCon, Ludwigsburg, Germany**
       * October 16 - 19, 2023
       * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
       * Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects
       * CFP is currently open until June 16.


### Gaia-X 

   * Frederik Tengg introduces the current developments towards the Gaia-X Academy
   * Strategy and concept currently being developed
   * Focus on "Training \& Academy" and "Certifications"
   * Aiming at 
       * Fundamentals
       * Gaia-X Advanced for Developers, Architects incl. Certifications
       * Point of contact for the Gaia-X Academy: frederik.tengg@gaia-x.eu
   * Addressing the public out there with courses for all focus groups until end of year (members only!)
   * Depending on the input of the OSS community to provide content tailored to the needs of the community
   * Opening up for discussion, depending on the BoD, if there shall be access for the non-member community
   * Relevant for the market adoption
   * First steps will be published as soon as they are available, content will be sliced to make it easier to participate
       * Collaboration not through e-mail ping-pong does not scale too well
       * Looking to ideas on how to funnel contributions of non-members
       * Topics are being collected
       * Gamification currently not in it, feedback appreciated and tbd


### Introduction to Nautilus for Pontus-X

   * **Nautilus, a JS wrapper / API for Pontus-X and Ocean Protocol**, released to enable automatic publication and consumption in the public Pontus-X ecosystem for Gaia-X.
       * Enables easy integration in other applications
       * Repository: [https://github.com/deltaDAO/nautilus](https://github.com/deltaDAO/nautilus)
       * [https://deltadao.github.io/nautilus/docs/api/](https://deltadao.github.io/nautilus/docs/api/)
   * Feel free to reach out to the team at any point in time to get you started, trying it out or contributing
   * Code under Apache 2 license.


### Updates from other lighthouses / projects / developers?

   * **Gaia-X LAB and Gaia-X Digital Clearing Houses**
       * New way of being informed of changes in GXDCH: #releases on Slack. Changelogs published there on releases as well as on GitLab (tags + releases).
       * Wizard "step-by-step" release coming soon (with Lab hosted DIDs and derivated key pairs): [https://wizard.lab.gaia-x.eu/development](https://wizard.lab.gaia-x.eu/development) - [https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool)




### AOB



#### **Task for the Community**



**Express your expectations to the OSS-Community meeting (via mailing list, Slack/Matrix, here in the pad)**



   * What information are you looking for?
   * What do you expect?
   * How can we make this more useful?



