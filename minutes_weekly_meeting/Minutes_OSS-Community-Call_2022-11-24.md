# Meeting notes Gaia-X OSS community November 24, 2022


## Focus of the weekly

   * Gaia-X Summit


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. AOB



## General Notes

**Participants in the call:** 18

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to the minutes:** [https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * George Sidiras


**Useful resources for newcomers:**

   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)


### Interesting sessions, events and news

**We're also collecting all upcoming events here:** [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)



**Gaia-X Summit - Recap**



   * Slides for the Demo available in the DCP, [https://community.gaia-x.eu/f/15898606](https://community.gaia-x.eu/f/15898606) 
   * Video recording is being worked on, might not be available  due to quality issue
   * Repository: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario) 
   * The catalogue demo was closed after the summit. [https://webcatalog.gaia-x.community/catalog?](https://webcatalog.gaia-x.community/catalog?)
   * Enrich the catalogue with the data submitted before the summit. Only with members who participate to the first call for action:
       * Option1: Manually reconstruct data from the spreadsheet files, but data is missing.
       * Option2: release a wizard
       * Integrate the SD generated during the hachathon #5
           * [https://example-storage.lab.gaia-x.eu/self-descriptions/](https://example-storage.lab.gaia-x.eu/self-descriptions/) + <id>
   * Reopen the catalogue in Read-Only (early December – to be confirmed 24/11/2022)
       * With SD’s dummy signatures
   * Reopen the catalogue with integrated wizard (end of January)
       * With real signatures
           * eiDAS (like EU login [https://ecas.ec.europa.eu/cas/login?](https://ecas.ec.europa.eu/cas/login?) + "sign with eID") / EV SSL (react native application)
       * With multiple issuers per RDF object (“sameAs”)
   * More TC update here [https://community.gaia-x.eu/index.php/f/15897666](https://community.gaia-x.eu/index.php/f/15897666)
   * GXFS will transition to Eclipse in Q1 2023


**Past and Upcoming Events**



   * **European Big Data Forum**
       * Nov 21 - 23, Prague (CZ)
       * Link: [https://european-big-data-value-forum.eu/2022-edition/programme/](https://european-big-data-value-forum.eu/2022-edition/programme/)


   * **SCS Hackathon happened**
       * Nov 22
       * [https://scs.community/2022/11/15/hackathon-preparation/](https://scs.community/2022/11/15/hackathon-preparation/)


   * **Mobility Meetup with the Gaia-X Hub Netherlands and the Dutch Blockchain Coalition**
       * Nov 24
       * [https://dutchblockchaincoalition.org/en/agenda/decentralized-sustainability-event-berlin](https://dutchblockchaincoalition.org/en/agenda/decentralized-sustainability-event-berlin)


   * **GC Tech-Talk No. 6**
       * „Secure Operating System and Test Automation - lessons learned and future perspectives“ (Tor Lund-Larsen, Cyberus Technology)
       * Nov 24, 2 - 3 pm, online
       * Link: [https://mautic.cloudandheat.com/techtalk-registration](https://mautic.cloudandheat.com/techtalk-registration)


   * **GXFS Roadshow**
       * Nov 30, Physical Attendance (Frankfurt)
       * [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)


   * **Gaia-X Roadshow powered by eco**
       * Nov 30 in Frankfurt, date for Hamburg will follow soon, Viena-Austria: Mar 16 2023, 
       * Link: [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)


   * **Data Spaces Support Centre**
       * Dec 1
       * Link: [https://dssc.eu/starter-kit/](https://dssc.eu/starter-kit/)


   * **Open Source Workshops for Computing \& Sustainability**
       * Dec 2, Brussels (BEL)
       * Location: At Albert Borschette Congress Center (CCAB)
       * Organised: By the European Commission in collaboration with the SWForum.eu Coordination and Support Action
       * Link: [https://www.swforum.eu/events/open-source-workshops-computing-sustainability](https://www.swforum.eu/events/open-source-workshops-computing-sustainability)


   * **Event by Gaia-X Finland** 
       * "Gaia-X Finland: Data Sharing for Breakfast" and "Data Spaces Technology Landscape 2023"
       * Dec 14, Helsinki (FIN)
       * Link: [https://www.gaiax.fi/](https://www.gaiax.fi/)
       * [https://www.sitra.fi/en/events/data-spaces-technology-landscape-2023/#speakers-of-the-event](https://www.sitra.fi/en/events/data-spaces-technology-landscape-2023/#speakers-of-the-event)
 

### AOB

   * Gaia-X Demo and Deployment Scenario [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario)







