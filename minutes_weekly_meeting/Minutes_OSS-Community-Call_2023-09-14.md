# Meeting notes Gaia-X OSS community September 14, 2023

## Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Demo: the latest updates in the Trust Framework Implementation
   6. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   7. AOB


##  Focus of the weekly

   * Demo: the latest updates in the Trust Framework Implementation

## General Notes

   * **Participants in the call:** 41
   * **Acceptance of Agenda:** Yes
   * **Acceptance of last week's minutes:** Yes
   * **Link to last week's minutes:**[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-09-07.md](**https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-09-07.md**)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Bruno Pacheco, Software Engineer, Luxembourg National Data Service
   * Gerald Ristow - Software AG, EuProGigant


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**SODA DATA Vision 2023** (in parallel with **Open Source Summit Europe** | Linux Foundation Events 19-21 September)

   * September 18, Bilbao
   * Call for proposals - until July 31st. Hybrid.
   * [https://www.sodafoundation.io/events/sodadatavision2023/](https://www.sodafoundation.io/events/sodadatavision2023/)
   * Topics (among others):
       * Data Catalog for Files and Object
       * Cloud Native Data Management
       * Hybrid Multi-Cloud Data Lifecycle \& Protection


**Members Monthly Updates Webinar**

   * 22 Sep 2023
   * 13:30 – 14:15
   * Microsoft Teams 
   * For Gaia-X members only
   * [https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/21](https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/21)


**Data Space Business Committee Event**

   * 23 Oct 2023
   * 11:00 – 13:00
   * [https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/24](https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/24)


**Gaia-X Roadshow** 

   * October 10, 6 - 11pm, Wolfsburg (Germany)
   * Registration: [https://www.eco.de/event/gaia-x-roadshow-wolfsburg](https://www.eco.de/event/gaia-x-roadshow-wolfsburg) 


**EuProGigant Open House Day**

   * October  10 - 11, Berlin (Germany)
   * EuProGigant ecosystem of five projects
   * Registration open, 250 PAX, physical participation only
   * [https://euprogigant-openhouseday2023.b2match.io/](https://euprogigant-openhouseday2023.b2match.io/)


**Future Congress Digital** 

   * October 11, Wolfsburg (Germany)
   * Gaia-X and Digital Mobility Ecosystems
   * [https://www.futurecongress.digital/anmeldung](https://www.futurecongress.digital/anmeldung)


**EclipseCon**

   * October 16 - 19, Ludwigsburg (Germany)
   * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
   * Workshop "Federation Services @ Eclipse" during the community day on Monday, October 16th 
       * [https://www.eclipsecon.org/2023/community-day](https://www.eclipsecon.org/2023/community-day)
   * Dedicated workshop "Federation Services" at the first day of the week. 


**GXFS Tech Workshop**

   * Registration and Agenda [https://www.eco.de/event/gxfs-tech-workshop-4/](https://www.eco.de/event/gxfs-tech-workshop-4/) 
   * Location: INNOPORT Reutlingen / Max-Planck-Straße 68/1/ 72766 Reutlingen
   * Date: 17.10.2023
   * Topics:
       * „Was tragen Initiativen wie Gaia-X und Manufacturing-X zur Digitalisierung der Industrie bei? - In German
       * Hands on with the WFE - In English 


**Gaia-X Summit 2023**

   * November 9-10, Alicante (Spain)
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


### Demo: the latest updates in the Trust Framework Implementation

   * Presented by Ewann
   * Link to the presentation: [https://membersplatform.gaia-x.eu/?locale=en#/public-event-details/22](https://membersplatform.gaia-x.eu/?locale=en#/public-event-details/22)
   * [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * [https://compliance.lab.gaia-x.eu/](https://compliance.lab.gaia-x.eu/)
   * [https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)
   * NON-PROD:  [https://ces-development.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/get\_credentials\_events](https://ces-development.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/get\_credentials\_events)
   * PROD: [https://ces-v1.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/get\_credentials\_events](https://ces-v1.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/get\_credentials\_events)


### Updates from other lighthouses / projects / developers / lab?

   * **GXFS Connect Wrap and Review by Andreas**
       * [https://www.linkedin.com/pulse/gxfs-connect-2023-technology-community-decides-success-andreas-weiss%3FtrackingId=VvkM0xb3S9yUyHIHjQEX2w%253D%253D/?trackingId=VvkM0xb3S9yUyHIHjQEX2w%3D%3D](https://www.linkedin.com/pulse/gxfs-connect-2023-technology-community-decides-success-andreas-weiss%3FtrackingId=VvkM0xb3S9yUyHIHjQEX2w%253D%253D/?trackingId=VvkM0xb3S9yUyHIHjQEX2w%3D%3D)
   * **deltaDAO and Pontus-X join European Commission Blockchain Regulatory Sandbox, endorsed by Gaia-X AISBL.**
       * [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/Sandbox+Project](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/Sandbox+Project)
       * See: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSISANDCOLLAB/European+Blockchain+Sandbox+announces+the+selected+projects+for+the+first+cohort#EuropeanBlockchainSandboxannouncestheselectedprojectsforthefirstcohort-deltaDAO](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSISANDCOLLAB/European+Blockchain+Sandbox+announces+the+selected+projects+for+the+first+cohort#EuropeanBlockchainSandboxannouncestheselectedprojectsforthefirstcohort-deltaDAO)
       * The European Blockchain Sandbox for innovative use cases involving Distributed Ledger Technologies (DLT) is an initiative of the European Commission. The aim of the project is to provide a framework for regulators, supervising authorities and blockchain innovators to engage in regulatory dialogue.
       * Blockchain and other Distributed Ledger technologies are among the important innovative developments in recent years with wider utility beyond crypto assets. Blockchain use cases and applications are deployed increasingly in industry sectors such as energy \& utilities, education, healthcare, mobility, finance \& insurance, telecommunications \& IT and logistics \& supply chains. Updated information on the Blockchain ecosystem developments in the EU/EEA can be found on the website of the European Blockchain Observatory and Forum, a European Commission initiative to accelerate blockchain innovation and the development of the blockchain ecosystem within the EU and so help cement Europe’s position as a global leader in this transformative new technology.
       * The sandbox will annually accept cohorts of 20 blockchain use cases. They will be matched with relevant national and EU regulators for a safe and constructive dialogue on the most relevant regulatory issues. Use cases will be selected on the basis of the maturity of the business case, legal/regulatory relevance and their contribution to the EU’s wider policy priorities.
       * The European Commission funds the facilitator of the sandbox under the Digital Europe Programme. The sandbox delivers on the Commission’s SME Strategy for a sustainable and digital Europe.
       * Great to see Fraunhofer IML, Eviden, EBSI, Commissariat à l'énergie atomique, twinu and many others as well in the first cohort.
   * **COOPERANTS aerospace lighthouse joins Pontus-X**
       * COOPERANTS is the world’s only collaborative alliance of industry, SMEs and research institutes in the Aeronautics and space sector that solves pressing digital collaboration problems by creating a common data space. For this purpose, COOPERANTS develops and operates a digital infrastructure according to Gaia-X standards for the low-barrier exchange of heterogeneous data from individual system landscapes as well as for the operation of new, collaborative smart services. [https://cooperants.de/en/](https://cooperants.de/en/)
       * Article: [https://cooperants.de/en/pioneering-the-future-of-aerospace-digitization-with-deltadao-cooperants-joins-pontus-x/](https://cooperants.de/en/pioneering-the-future-of-aerospace-digitization-with-deltadao-cooperants-joins-pontus-x/)
       * We are particularly proud to unveil the integration of two new Cooperants marketplaces and Gaia-X portals into the Pontus-X ecosystem: one dedicated to the COOPERANTS project and the other tailored for one consortium member, Airbus Defence and Space, a first example to be set for and to be followed by other members of the consortium. Beyond enhancing interoperability with existing Gaia-X lighthouse projects, such as EuProGigant and Gaia-X4FutureMobility moveID, this integration marks a significant stride towards dismantling data silos and fostering a unified European market. 
       * New features and services can already be discovered there, but most services are limited to consortia members, so you will only be able to see limited metadata from the catalogue.
       * [https://cooperants.pontus-x.eu/](https://cooperants.pontus-x.eu/)
       * [https://airbus.pontus-x.eu/](https://airbus.pontus-x.eu/)
       * [https://portal.euprogigant.io/](https://portal.euprogigant.io/)
       * [https://portal.moveid.eu/](https://portal.moveid.eu/)
   * **Servicemeister Portal and AI integration + Road to XFSC cross ecosystem interoperability**
       * Service-Meister is to develop an AI-based, cross-plant, cross-department, and cross-company service platform for German SMEs. An important subgoal is to enable less-qualified staff to offer complex services with the help of digital guides, such as AI-based ServiceBots and Smart Services.
       * A further subgoal is to provide digital service knowledge on a single platform, which will enable cross-company scalability of service. This will evolve into a service ecosystem which will counteract the skills shortage in Germany and will keep German SMEs competitive in the long-term.
       * [https://www.servicemeister.org/en/](https://www.servicemeister.org/en/)
       * The new Servicemeister Portal is being launched today [https://servicemeister.pontus-x.eu/](https://servicemeister.pontus-x.eu/) and we are proud to unveil this cooperation between Service-Meister, eco, XFSC and Pontus-X + deltaDAO
       * AutoTiM application by innovex \& Krohne supports ML and classification/prediction on a number of selected use cases free to try and assess for production usage in your institution
       * Next XFSC identity components will be integrated and we look further at catalogue-sync with the XFSC catalogue and of course Gaia-X "CES / Las Vegas" service for catalogue sync
   * **XFSC Catalogue weekly**
       * Following up on the discussions at []GXFS Connect[], XFSC community / Christoph Lange will set up a regular bi-weekly "Catalogue Developers' Community Call" to systematically address points such as:
       * What bugs/features (i.e., issues in the repository) are needed most urgently and should thus be prioritized?
       * Who (e.g., a team from some Gaia-X project) is going to implement them?
       * What is the state of implementation (e.g., reviewing merge requests).
       * See: [https://gaia-xworkspace.slack.com/archives/C012EEV7Q93/p1694670719460609](https://gaia-xworkspace.slack.com/archives/C012EEV7Q93/p1694670719460609)
       * Slack channel: [https://gaia-xworkspace.slack.com/archives/C01L0HBDBC0/p1694672563170159](https://gaia-xworkspace.slack.com/archives/C01L0HBDBC0/p1694672563170159)
       * Matrix Chat: [https://app.element.io/#/room/](https://app.element.io/#/room/)!flmTthUebPfZFnEyxM:matrix.org
 

### AOB

   * Community input/support for submitting a proposal for Next Generation Internet [https://trustchain.ngi.eu/open-call-2/](https://trustchain.ngi.eu/open-call-2/) (deadline 'sportlich' **20 September**).
   * Idea: further develop the Unigrid Decentralized Internet ([https://unigrid.org)](https://unigrid.org)) while ensuring maximum interoperability / standardization, e.g. in line with Gaia-X, maybe EBSI, etc. (René Krikke, engage@mrprobot.com - not sure if I can make the meeting).

**Announcement for next week**

   * Gaia-X WG 5: Sustainable Open Mobility Services (SOMS)
       * New project application in the Gaia-x 4 Future Mobility
       * Plan: Hand in proposal End of 2023
       * Implementation of real time Life Cycle Assessment to evaluate environmental impacts in Gaia-X 
       * Open Mobility Services evaluate vehicle operation based on Digital Twin data
       * Consortium 
           * contains: A Component and technology supplier, an LCA software developer, an interface programmer, the city of Hamburg, the DIN and two potential fleet owners
           * lacks: Eventually a fleet provder for implementing use cases, Software developer for the OMS
       * Important aspects for OMS / Interfaces: Open source and interoperability! (e.g. compatibility between different services)
       * contact: [https://matrix.to/#/@nilez:matrix.org](https://matrix.to/#/@nilez:matrix.org)



















