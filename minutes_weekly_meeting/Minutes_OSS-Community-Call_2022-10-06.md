# Meeting notes Gaia-X OSS community October 06, 2022



## Focus of the weekly

   * Upcoming events
   * OSS Community wishes <3
   * Gaia-X SD Experiment


## Proposed Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of Agenda

3. Introduction of new participants and regular contributors 

4. Interesting sessions, events and news

5. Organizational topics

6. Work on the Deliverables

   * 6.1 Release of the Gaia-X-Open-Source-Software GXFS
   * 6.2 Demonstrations
7. AOB 



## General Notes

Participants in the call: 23

Agenda: Accepted, no points added.



**Please note:** Kai will be absent the next two weeks; in case of questions etc please reach out to Cristina! :)



### Introduction of new participants and regular contributors

   * Ute Brönner


**Useful resources for newcomers:** The Gaia-X Framework. [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)



### Interesting sessions, events and news

   * **Gaia-X Summit 2022**
       * Nov 17 - 18, Online Attendance (unlimited), Physical Attendance (very limited)
       * Link: [https://gaia-x.eu/event/gaia-x-summit-2022/](https://gaia-x.eu/event/gaia-x-summit-2022/)


   * **Gaia-X Roadshow powered by eco**
       * Oct 20 in Munich, Nov 30 in Frankfurt, date for Hamburg will follow soon
       * Link: [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)


   * **Hackathon No. 5 Results**
       * Link: [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home#final-result-presentations](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home#final-result-presentations)


   * **EuProGigant Open House Day in Vienna \& Smart and Networked Manufacturing Congress**
       * Oct 5 - 6
       * Link: [https://www.eitmanufacturing.eu/news-events/events/5th-vienna-production-congress-smart-networked-manufacturing/](https://www.eitmanufacturing.eu/news-events/events/5th-vienna-production-congress-smart-networked-manufacturing/)


   * **MiCA**
       * Proposal for a  Regulation of the European Parliament and of the Council of Markets in Crypto-assets and amending Directive (EU) 2019/1937)
       * Link: [https://data.consilium.europa.eu/doc/document/ST-13198-2022-INIT/en/pdf](https://data.consilium.europa.eu/doc/document/ST-13198-2022-INIT/en/pdf)


   * **Open Source Experience**
       * Nov 8 - 9
       * Link: [https://www.opensource-experience.com/en/](https://www.opensource-experience.com/en/)


### Work on the Deliverables



#### Wishes, ideas, feedback from the community.



   * Overview of session events and news.
   * Tech demos
   * GXFS demos
   * Would a reoccuring demonstration of the compliance service be useful to the community. This could also be announced before the session so people interested can join and people who already know it can skip it.
   * Demonstrations of core software would be useful
   * I see this group as the one which would be discussing on latest developments, finding code base (yes, even finding), info on hands-on experimentations, demos, experiences.
   * I would like to hear about success stories - GaiaX projects which are already in production because there are a lot of projects in this area, but it is difficult to understand what is done currently and what is still in draft/ in progress.
   * Eduard inviting upstream projects to share how one could engage in the projects and discuss about what they actually need to foster the project
