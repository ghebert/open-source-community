
   * Gaia-X OSS Community Call - February 1st
Next call: Thursday, February 8th, 2024, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)



## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


## Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Meeting notes

### General meeting notes

#### Acceptance of agenda

yes



#### Number of participants

38



#### Acceptance of last meeting notes

yes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240125\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240125\_meeting\_notes.md)



### Introduction of new participants

Participant name, institution, position, project



### Interesting sessions, events, and news

recent: 

   * 



next:

   * Event name: Cross Cloud Spaces Workshop 2024
       * Date: 26 February 2024,
       * Location  Barcelona
       * Event link: [https://web-eur.cvent.com/event/ba2eba73-ee72-4a1e-8ff8-cddb962fa05a/summary?locale=en](https://web-eur.cvent.com/event/ba2eba73-ee72-4a1e-8ff8-cddb962fa05a/summary?locale=en)
   * Data Spaces Symposium and Market-X
       * March 12-13, 2024, Darmstadt, Germany
       * [https://gaia-x.eu/market-x-2024-agenda/](https://gaia-x.eu/market-x-2024-agenda/)
       * There will be some fees, Gaia-X members will likely to attend for free, to be confirmed
       * Tech stage on 12th March Morning, agenda published (Gaia-X 101, IPFS, Policy reasoning \& trusted computing)
   * Hannover Fair 2024
       * April 22-26, 2024, Hannover, Germany
       * [https://www.hannovermesse.de/de/](https://www.hannovermesse.de/de/)
   * Sovereign Cloud Stack Summit 2024
       * [https://scs.community/summit2024/](https://scs.community/summit2024/)
       * Tue, May 14, Berlin (mostly German language content, strategic)
       * Co-located with OpenInfra Days (technical) on May 15 (mostly English)




### Updates and questions from lab, lighthouses, projects, and developers

   * Lab: C4 Model Architecture in progress: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/docs/architecture-c4-model/architecture/architecture.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/docs/architecture-c4-model/architecture/architecture.md?ref\_type=heads)
   * Lab: digging around SGX and trusted computing (Trying to run compliance in enclave)
   * Lab: Extracting of VC signature and verification in a library, to support "our" signature, and the one defined in the specs test bed. @Vincent (issue: [https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/issues/53#note\_1738416999)](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/issues/53#note\_1738416999))
       * Robert interested in a Java implementation for XFSC Catalog
       * Python mentioned too by Veronika
   * Lab: Asked the community regarding evolution in the CES. 
       * One large breaking change, changing cloud events of VC to cloud events of VP
       * One slightly smaller change, allowing any VC signed (from EVSSL:eIDAS:SSL) to be pushed in CES, and addition of type filtering (MR : [https://gitlab.com/gaia-x/lab/credentials-events-service/-/merge\_requests/15/)](https://gitlab.com/gaia-x/lab/credentials-events-service/-/merge\_requests/15/))
       * There are some concerns about information processing, storage and exposition (from Pietro Bartoccioni) those need to be checked by the CTO Team before implementing a solution, the subject will be followed on [https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/issues/58](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/issues/58) 




### AOB

-



## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)

















