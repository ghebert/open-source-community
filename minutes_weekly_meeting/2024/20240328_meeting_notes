
   *  Gaia-X OSS Community Call - March 28
Next call: Thursday, April 4th, 2024, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240321\_meeting\_notes](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240321\_meeting\_notes)





Moderation: Kai  Meinke

Minutes of meeting : collective 



## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Request to provide input to an overview of existing OSS components and repositories
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Verifiable Credential Signing Solution Proposal by Gaia-X CTO Team
   1. Demonstration of ODRL Policy usage by GXFS-FR
   1. Updates from the lab, lighthouses, projects and developers


## Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Meeting notes

### General meeting notes

#### Acceptance of agenda

yes



#### Number of participants

60



#### Acceptance of last meeting notes

yes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240321\_meeting\_notes?ref\_type=heads](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240321\_meeting\_notes?ref\_type=heads)



### Overview of existing OSS components to build digital infrastructure and data ecostems

Klaus Ottradovetz is compiling a list of existing OSS components / repositories that can be used to build (Gaia-X based) distributed digital (data) ecosystems. Please contact him directly at klaus.ottradovetz@eviden.com or @kottradovetz:matrix.org; Result will be presented to this community



Introduction of new participants

   * participant, affiliation, project
       * none today


### Interesting sessions, events, and news

next:

   * Tech deep dive, 15:00 CET
       * March 28th, hosted by Ewann from the Lab team
       * [https://gaia-x.eu/event/tech-deep-dive-2/](https://gaia-x.eu/event/tech-deep-dive-2/)
       * Trusted execution for the Gaia-X components
   * Hannover Fair 2024
       * April 22-26, 2024, Hannover, Germany
       * [https://www.hannovermesse.de/de/](https://www.hannovermesse.de/de/)
       * If you want to register for free tickets, in relation to the German Gaia-X Hub, [https://www.hannovermesse.de/de/applikation/registrierung/direkteinstieg-tickets-ausweise?code=rXzKm](https://www.hannovermesse.de/de/applikation/registrierung/direkteinstieg-tickets-ausweise?code=rXzKm)
   * Sovereign Cloud Stack Summit 2024
       * [https://scs.community/summit2024/](https://scs.community/summit2024/)
       * Tue, May 14, Berlin (mostly German language content, strategic)
       * Co-located with OpenInfra Days (technical) on May 15 (mostly English), CfP closing Mar 28: [https://oideurope2024.openinfra.dev/?\_ga=2.81567767.1664958746.1708893806-1894249939.1705129736#registration=1](https://oideurope2024.openinfra.dev/?\_ga=2.81567767.1664958746.1708893806-1894249939.1705129736#registration=1)
   * Cloud Expo Frankfurt 
       * May 22-23, Frankfurt, Germany
       * Gaia-X will be there with the German Gaia-X Hub
       * [https://www.cloudexpoeurope.de/](https://www.cloudexpoeurope.de/)
   * Gaia-X Tech-X and Gaia-X Hackathon No. 7
       * May 23-24, European Convention Center Luxembourg, hosted by Gaia-X Hub Luxembourg
       * CFP opens Thursday, 14 March, presentations \& hacks
           * Please submit proposal by Thursday, 18 April, info on the wiki page below.
       * More informations about this on the Wiki page here 👉 [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/wikis/home)
   * XFSC Tech Workshop #7 
       * Location : Hürth, Germany (close to Cologne)
       * Date: 12 June 2024
       * Registration: [https://www.gxfs.eu/xfsc-tech-workshop-7/](https://www.gxfs.eu/xfsc-tech-workshop-7/) 
       * Agenda coming – showcase the latest advancements in XFSC Specification Phase 2  ([https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)](https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)) 


### New GXDCH/Compliance Release

   * will be based on OIDC4VP Credential Exchange and new APIs to align with existing credential exchange protocols. Draft 13 could become the stable standard for OIDC4VCI, for  exchange it will be a different version. Final state awaited.
   * This will also help to align the different wallets used by initiatives and lighthouses towards interoperability
   * Heavy breaking changes to be expected. Will also be shared as part of the Eclipse Trust Protocol Working Group
   * Other wallets than mobile wallets needed as mobile wallets won't address all the requirements, especially when it comes to organizational credential managers
   * If you are aware of existing projects which could be leveraged here, please make proposals. 
   * Usual suspects are already known: walt (draft 8, upgrade needed), Talao/Altme, Sphereon, XFSC, MIW ...


### Verifiable Credential Signing Solution Proposal

   * Presented by Vincent Kelleher <vincent.kelleher@gaia-x.eu> on behalf of the Gaia-X CTO Team
   * The current Json Web Signature 2020 signature method is deprecated
   * Issues have started to appear with the use of the above
   * This presentation will explain the current signature method, expose available standards for signing verifiable credentials and propose the use of one of those standards (VC-JWT)
   * Slides available here 👉 https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes_weekly_meeting/2024/presentations/OSS%20Meeting%2028032024%20-%20VC%20Proof%20Proposal.pdf
   * An issue has been opened to discuss this 👉 https://gitlab.com/gaia-x/lab/gxdch/-/issues/19


### Demonstration of ODRL Policy usage by GXFS-FR (postponed to next week)

   * Presentation by Guillaume HEBERT and Hoan HANG
   * Consumption of a DataProduct with rights constraints expressed in ODRL
   * ODRL rules on DataProduct and DataContract negotiated between a DataProvider and a DataConsumer can be translate to a Amazon S3 Policy to be understandable by "traditional Authorization" system.


### Updates from the Community, Developers, Lighthouses and Lab

   * out of time


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)





