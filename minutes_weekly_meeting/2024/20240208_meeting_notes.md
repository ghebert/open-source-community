
   * Gaia-X OSS Community Call - February 8th
Next call: Thursday, February 15th, 2024, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024?ref\_type=heads](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024?ref\_type=heads)



## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


## Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Meeting notes

### General meeting notes

#### Acceptance of agenda

yes



#### Number of participants

44



#### Acceptance of last meeting notes

yes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240201\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240201\_meeting\_notes.md)



### Introduction of new participants

   * Urtza, Tecnalia
   * Hylke van der Schaaf, Fraunhofer, 
   * Luis, 
   * Paul Massey, Profile.io professional network


### Interesting sessions, events, and news

recent: 

   * Article on Trust and Transparency Index: [https://gaia-x.eu/news-press/gaia-x-and-trust-indexes/](https://gaia-x.eu/news-press/gaia-x-and-trust-indexes/)
   * Prometheus-X launch: [https://catalog.visionstrust.com/public/catalog/datatypes](https://catalog.visionstrust.com/public/catalog/datatypes)


next:

   * Event name: Cross Cloud Spaces Workshop 2024
       * Date: 26 February 2024,
       * Location  Barcelona
       * Event link: [https://web-eur.cvent.com/event/ba2eba73-ee72-4a1e-8ff8-cddb962fa05a/summary?locale=en](https://web-eur.cvent.com/event/ba2eba73-ee72-4a1e-8ff8-cddb962fa05a/summary?locale=en)
   * Data Spaces Symposium and Market-X
       * March 12-14, 2024, Darmstadt, Germany
       * [https://gaia-x.eu/market-x-2024-agenda/](https://gaia-x.eu/market-x-2024-agenda/)
       * [https://www.data-spaces-symposium.eu/program](https://www.data-spaces-symposium.eu/program)
       * There will be fees, Gaia-X members will be able to attend the Market-X day, March 12, for free
           * Tickets should be available on February 9
           * Two other days around 100 euros / day
       * Tech stage on 12th March Morning, agenda published (Gaia-X 101, IPFS, Policy reasoning \& trusted computing)
   * Hannover Fair 2024
       * April 22-26, 2024, Hannover, Germany
       * [https://www.hannovermesse.de/de/](https://www.hannovermesse.de/de/)
   * Sovereign Cloud Stack Summit 2024
       * [https://scs.community/summit2024/](https://scs.community/summit2024/)
       * Tue, May 14, Berlin (mostly German language content, strategic)
       * Co-located with OpenInfra Days (technical) on May 15 (mostly English)
   * Cloud Expo, 
       * May 23-24, Frankfurt, Germany
       * Gaia-X will be there with the German Gaia-X Hub
   * Tech-X, May 16-17 tbc, Gaia-X Hub Luxembourg




### Updates and questions from lab, lighthouses, projects, and developers

   * DID Library: [https://gitlab.com/gaia-x/lab/did-web-generator](https://gitlab.com/gaia-x/lab/did-web-generator) [https://www.npmjs.com/package/@gaia-x/did-web-generator](https://www.npmjs.com/package/@gaia-x/did-web-generator)
   * JsonWebSignature2020 Library available: [https://gitlab.com/gaia-x/lab/json-web-signature-2020](https://gitlab.com/gaia-x/lab/json-web-signature-2020)  [https://www.npmjs.com/package/@gaia-x/json-web-signature-2020](https://www.npmjs.com/package/@gaia-x/json-web-signature-2020)
   * WIP to integrate signature lib to compliance V1 to accept "standard" signature
   * Lab: C4 Model Architecture in progress: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/docs/architecture-c4-model/architecture/architecture.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/docs/architecture-c4-model/architecture/architecture.md?ref\_type=heads)
   * CES: Evolution to accept customVC ids ready [https://gitlab.com/gaia-x/lab/credentials-events-service/-/merge\_requests/15/](https://gitlab.com/gaia-x/lab/credentials-events-service/-/merge\_requests/15/)
       * For privacy, the "non-gaia-x" events need to only contain a subject with the ID of the custom VC.
       * Expect a V2 with VP instead of VC in the cloud Event
   * Pontus-X ecosystem sees new data space initiatives in manufacturing, Flex4Res, Dione-X, ACCURATE extending also the EuProGigant ecosystem
       * New newsletter and community events : [https://newsletter.pontus-x.eu/](https://newsletter.pontus-x.eu/)
   * Pontus-X and Ocean Protocol working on the SHACL shapes for the custom-VCs and next version of strictly enforced credential schemata in Gaia-X.
       * Example: [https://github.com/GX4FM-Base-X/pontus-x-ontology](https://github.com/GX4FM-Base-X/pontus-x-ontology), [https://github.com/deltaDAO/pontus-x-docs/blob/main/static/pontus-x-ontology/ttl/ocean\_4.5.0.ttl](https://github.com/deltaDAO/pontus-x-docs/blob/main/static/pontus-x-ontology/ttl/ocean\_4.5.0.ttl)
       * Example: Eth Ontology [https://ethon.consensys.io/prop-address.html](https://ethon.consensys.io/prop-address.html)
       * Lots of work with the playgrounds ahead. Any tips by the community for efficient development and testing?
           * [https://shacl.org/playground/]([]https://shacl.org/playground/[]) + [https://shacl-playground.zazuko.com/]([]https://shacl-playground.zazuko.com/[])
           * [https://sd-creation-wizard.gxfs.gx4fm.org/form](https://sd-creation-wizard.gxfs.gx4fm.org/form)
           * [https://json-ld.org/playground/](https://json-ld.org/playground/)


### AOB

-



## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)

















