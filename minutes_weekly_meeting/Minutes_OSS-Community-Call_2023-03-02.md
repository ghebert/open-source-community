# Meeting notes Gaia-X OSS community March 02, 2023

## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Tech-X \& Hackathon #6

6. Updates from other lighthouses / projects / developers

7. AOB

## Focus of the weekly

   * Updates from the community
   * Hackathon No. 6


## General Notes

**Participants in the call:** 27

**Acceptance of Agenda:** Agenda accepted.

**Acceptance of last week's minutes:** Agenda accepted.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-02-23.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-02-23.md) 

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * Souvik Sengupta (Senior Lead Researcher, Digital Ecosystems Solutions, IONOS SE) 
   * David Batalla


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)


### Interesting sessions, events and news

   * **FOSS Backstage 23**
       * March 14-15, Berlin (and online)
       * Theme: *Community, Management \& Compliance in Open Source Development*
       * [https://23.foss-backstage.de/](https://23.foss-backstage.de/)
       * Eduard \& Ariane will be there to present **Community2**
           * Community2 is our new community for Open Source Community Managers
           * More information: [https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop](https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop)


   * **Market-X, Vienna, Gaia-X Market Adoption Event**
       * March 14-15, Vienna, Aula der Wissenschaften, [https://www.aula-wien.at/](https://www.aula-wien.at/) 
       * Updated website: [https://gaia-x.eu/market-x/](https://gaia-x.eu/market-x/)


   * **New event series: GXFS Tech Workshops** (Key contact: vivien.witt@eco.de)
       * [https://www.gxfs.eu/events/](https://www.gxfs.eu/events/)
       * Please save the date for **15 and 16 March 2023**. 
       * The Identity \& Trust Workshop and the Self-Description Workshop will take place on these consecutive days. Location tbd


           * Before planning your trip, please note that knowledge in the following technical areas and access to specific tools are **[]mandatory[]**. If you do not have experience with these tools, a participation in the workshops will be difficult: 
               * Postman, Curl, Docker, Kubernetes, Github Access necessary
               * Javascript, Java, Go or any other programming language
               * Development IDE on the notebook (e.g. Visual Studio Code)
               * Gitlab, Minikube, k3s or kubernetes cluster are good to have 
               * IOS/Android Smartphone, Downloaded PCM App (we will be sharing instructions for the PCM download ahead of the workshop) 
           * Livestream also planned for passive participation


   * **Data Spaces Symposium \& Deep-Dive Day**
       * March 21–23, The Hague
       * Info and registration link: [https://internationaldataspaces.org/data-spaces-symposium]([]https://internationaldataspaces.org/data-spaces-symposium[])


   * **Hannover Messe, Hannover, Germany**
       * April 17-21, Hannover (five days is new in 23)
       * [https://www.hannovermesse.de/en/](https://www.hannovermesse.de/en/)
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de
       * Gaia-X Area


   * **Tech-X \& Hackathon #6**
       * May 3-4, Bilbao, Spain
       * On-Site event
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home)


   * **Cloud Expo Europe, Frankfurt, Germany**
       * May 10-11, Frankfurt
       * [https://www.cloudexpoeurope.de/themes2023](https://www.cloudexpoeurope.de/themes2023)
       * Gaia-X at the heart of the Expo, Gaia-X Arena 
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de


   * **MyData conference**
       * May 31 - June 01, Helsinki
       * [https://www.mydata.org/event/mydata-2023/](https://www.mydata.org/event/mydata-2023/)




### Tech-X \& Hackathon No. 6

   * **Infos and Guideline**
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home)


   * **Location and date**
       * Azkuna Zentroa in Bilbao, Spain on the 3\&4th of May 2023


   * **Tech-X \& Hackathon #6 on site event** 
   
        * [https://gaia-x.eu/tech-x/](**https://gaia-x.eu/tech-x/**)
       * The onboarding of the hacking sessions will be done virtually, the week before the hackathon. The goal is to  make sure the dev environment is setup before the 2-days hacking and newcomers get familiar with the project and the challenge at-hand
       * What's new?
           * Hackathon has the same structure/organization as the previous Hackathons (submit a proposal with a topic you'd like to see tackled) - however: This time it's a competition where you can win prizes!


   * **Registration**
       * [https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv](https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv)
       * Registration open until end of April (or until we reach capacity)


   * **Call for proposals**
       * If you have anything in mind for the Hackathon and/or the tech event, please add to the proposals page (or align on suggestions that have already been added, preferably via the mailing list) [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals)
       * Deadline for proposals: March 31, 2023


   * **Call for organizers**
       * If you would like to help organizing the Hackathon, please reach out on the mailing list or to Cristina!


   * **Next steps for March:** Work on the agenda!



## Updates from other lighthouses / projects / developers?

   * Lighthouses and projects as well as individual developers are encouraged to share updates from their work and progress.
   * Update from Ewann Gavard from the Gaia-X Lab:
       * New endpoints are available for the work done in the lab, and the available versions are deployed by branch
       * The current work in progress for 22.10 is done on the development branch
       * [https://registry.lab.gaia-x.eu/development/docs/](https://registry.lab.gaia-x.eu/development/docs/)
       * [https://compliance.lab.gaia-x.eu/development/docs/](https://compliance.lab.gaia-x.eu/development/docs/) 
       * The versioning of the software will be done using semver, so the version of the Trust Framework document will no longer be in the name of the software version. This information however will be included in the documentation
       * Follow/join the discussion: [https://gaia-x.atlassian.net/browse/LAB-317](https://gaia-x.atlassian.net/browse/LAB-317)


### AOB

   * No meeting on March 16!









