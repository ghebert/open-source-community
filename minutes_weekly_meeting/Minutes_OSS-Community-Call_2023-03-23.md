# Meeting notes Gaia-X OSS community March 23, 2023


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Tech-X \& Hackathon #6

6. Summary of the GXFS Tech Workshop

7. Summary  of the Pontus-X Web3 Ecosystem for Gaia-X and GEN-X network + Gaia-X 4 Future Mobility and EuProGigant lighthouses

8. Updates from other lighthouses / projects / developers   

9. AOB



## Focus of the weekly

   * Updates from the community
   * Hackathon No. 6


## General Notes

**Participants in the call:** 26

**Acceptance of Agenda:** Accepted by the audience.

**Acceptance of last week's minutes:** Accepted by the audience.

**Link to last week's minutes:**[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-03-09.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-03-09.md) 

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)

### Introduction of new participants and regular contributors

   * Jonathan Köster, PEAQ, Gaia-X 4 Future Mobility moveID


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)


### Interesting sessions, events and news

   * **GXFS Tech Workshops** (Key contact: vivien.witt@eco.de)
       * [https://www.gxfs.eu/events/](https://www.gxfs.eu/events/)
       * Online Session toward 12:00 to 14:00 CET, please contact Lauresha if you want to join
       * More GXFS Tech Workshops will follow. We will inform this group as soon as we have a new date. 
       * GXFS Migration to Eclipse, proposal to be submitted until end of March
       * Matrix Chat GXFS Tech: [https://matrix.to/#/](https://matrix.to/#/)!flmTthUebPfZFnEyxM:matrix.org?via=matrix.org
       * Registration for the GXFS Monthly Webinar taking place tomorrow: [https://register.gotowebinar.com/register/4648997275467962456](https://register.gotowebinar.com/register/4648997275467962456)


   * **ALASCA Tech-Talk No. 3**
       * March 30, 2-3 pm (online)
       * Topic: *Let's Build a Maintainable Network* *Fabric* (with Christoph Glaubitz, SysEleven)
       * More information \& registration: [https://alasca.cloud/alasca-tech-talk-3/](https://alasca.cloud/alasca-tech-talk-3/)
       * Recording of Tech-Talk No. 2 (Topic: SCS): [https://www.youtube.com/watch?v=w1Q7V3297MQ](https://www.youtube.com/watch?v=w1Q7V3297MQ)


   * **Hannover Messe, Hannover, Germany**
       * April 17-21, Hannover (five days is new in 23)
       * [https://www.hannovermesse.de/en/](https://www.hannovermesse.de/en/)
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de
       * Gaia-X Area


   * **Tech-X \& Hackathon #6**
       * May 3-4, Bilbao, Spain
       * On-Site event
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home)


   * **Cloud Expo Europe, Frankfurt, Germany**
       * May 10-11, Frankfurt
       * [https://www.cloudexpoeurope.de/themes2023](https://www.cloudexpoeurope.de/themes2023)
       * Gaia-X at the heart of the Expo, Gaia-X Arena 
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de


   * **SCS Summit, Berlin, Germany**
       * Berlin, May 23+24
       * [https://scs.community/summit](https://scs.community/summit)
       * German and English language content


   * **MyData conference**
       * May 31 - June 01, Helsinki
       * [https://www.mydata.org/event/mydata-2023/](https://www.mydata.org/event/mydata-2023/)

   * **Overview of the Market-X, Vienna, Gaia-X Market Adoption Event**
       * Gaia-X Clearing House GXDCH explained by Pierre Gronlier, CTO


### Tech-X \& Hackathon No. 6

   * **Infos and Guideline**
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home)


   * **Location and date**
       * Azkuna Zentroa in Bilbao, Spain on the 3\&4th of May 2023


   * **Tech-X \& Hackathon #6 on site event** 
   
        - [https://gaia-x.eu/tech-x/](**https://gaia-x.eu/tech-x/**) 
        - Reminder to **fill in the proposals in the wiki**, if you intend to participate
   * **Registration**
       * [https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv](https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv)
       * Registration open until end of April (or until we reach capacity)


   * **Call for proposals**
       * If you have anything in mind for the Hackathon and/or the tech event, please add to the proposals page (or align on suggestions that have already been added, preferably via the mailing list)
       * Link to proposals: [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals)
       * Deadline for proposals: **March 31, 2023**
       * You can still edit your proposal until 31st of March, so add it to the page to let us know you intend to come
       * The proposals will be evaluated during 3-7 April and results will be shared during that time-frame


   * **Want to promote the event?**
       * A promo package is now available
       * Link: [https://gaia-x.eu/wp-content/uploads/2023/03/Tech-X\_Promo\_Pack.pdf](https://gaia-x.eu/wp-content/uploads/2023/03/Tech-X\_Promo\_Pack.pdf)


   * Next steps for March: **Work on the agenda!**


## Summary of the Pontus-X Web3 Ecosystem for Gaia-X and GEN-X network + Gaia-X 4 Future Mobility and EuProGigant lighthouses

   * Gaia-X 4 Future Mobility Project Family now official Gaia-X Lighthouse Project, announced at Market-X
       * [https://www.gaia-x4futuremobility.dlr.de/](https://www.gaia-x4futuremobility.dlr.de/)


   * Closely following the latest Trust Framework and Compliance Service version of the Lab
       * Example: [https://v4.minimal-gaia-x.eu/verify?did=did:op:2e148b5c1ff12590c13bc018100743f0253f22f49cbaf7876612b2417f961789](https://v4.minimal-gaia-x.eu/verify?did=did:op:2e148b5c1ff12590c13bc018100743f0253f22f49cbaf7876612b2417f961789)
   * Core Upgrade to v4 has been completed
       * Live on: [https://v4.minimal-gaia-x.eu/](https://v4.minimal-gaia-x.eu/)
   * EUROe integration completed, seamless payment in EURO enabled
       * Example: [https://v4.minimal-gaia-x.eu/asset/did:op:2e148b5c1ff12590c13bc018100743f0253f22f49cbaf7876612b2417f961789](https://v4.minimal-gaia-x.eu/asset/did:op:2e148b5c1ff12590c13bc018100743f0253f22f49cbaf7876612b2417f961789)
       * Docs: [https://dev.euroe.com/docs/Stablecoin/contract-addresses](https://dev.euroe.com/docs/Stablecoin/contract-addresses)
       * Blog: [https://www.euroe.com/blog/euroe-to-be-used-as-settlement-token-in-the-gaiax-web3-ecosystem](https://www.euroe.com/blog/euroe-to-be-used-as-settlement-token-in-the-gaiax-web3-ecosystem)
       * TX Example: [https://explorer.genx.minimal-gaia-x.eu/tx/0xa9465d116db5acb15d32ba9c5e6a5cdea63f94fc55f6c2a6a851ab4ebe59c585](https://explorer.genx.minimal-gaia-x.eu/tx/0xa9465d116db5acb15d32ba9c5e6a5cdea63f94fc55f6c2a6a851ab4ebe59c585)
   * GraphQL integration completed to connect to APIs, databases and join queries across sources
       * Example: [https://v4.minimal-gaia-x.eu/asset/did:op:b4ee67dfe05f0585506b648c280459f72700b6023daf823fa819487254e1d448](https://v4.minimal-gaia-x.eu/asset/did:op:b4ee67dfe05f0585506b648c280459f72700b6023daf823fa819487254e1d448)
   * Federated Analytics and Learning Integration completed
       * Example Gaia-X 4 Future Mobility: [https://fa.mobility.demo.delta-dao.com/](https://fa.mobility.demo.delta-dao.com/)
       * Example EuProGigant: [https://partmatching.euprogigant.io/](https://partmatching.euprogigant.io/)


   * New members of GEN-X network
       * TU Vienna
       * Software AG
       * more to be announced in the next two weeks


## Compliance Service

Important: Breaking Change upcoming. Brace for change.

Rules will not change, format will change.


   * Note from CTO Office/Lab: Incoming breaking change in the compliance. SD will be tested against shape files and thus, must be modified to do so. Please see MR: [https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/merge\_requests/154](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/merge\_requests/154)  Currently working on examples and polishing up this. Note that this will again change after service-characteristics small rework from CTO Team: [https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/merge\_requests/165](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/merge\_requests/165)


### AOB

Nothing.














