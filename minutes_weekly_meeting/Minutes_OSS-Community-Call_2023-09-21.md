## Meeting notes Gaia-X OSS community September 21, 2023

### Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Demo of Credential Event Service - presented by Kai
   6. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   7. AOB


###  Focus of the weekly

   * Demo of Credential Event Service


### General Notes

   * **Participants in the call:** 52
   * **Acceptance of Agenda:** Yes
   * **Acceptance of last week's minutes:** Yes
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-09-14.md](**https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-09-14.md**)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Robert Schubert, msg, GX4FM PLC-AAD
   * John Hoffmann, accenture, Gaia-X 4 Future Mobility ROMS
   * Enrica Sposato, team enterprise telecom italia, part of Gaia-X and Structura-X 
   * Alexander Sohr, DLR, Gaia-X 4 Future Mobility ROMS
   * Elmar Brockfeld, DLR, Gaia-X 4 Future Mobility ROMS
   * Yannick Meinberg, Software AG, DIONE-X


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**GXFS Connect: Wrap \& Review (article by Andreas)**

   * [https://www.linkedin.com/pulse/gxfs-connect-2023-technology-community-decides-success-andreas-weiss%3FtrackingId=VvkM0xb3S9yUyHIHjQEX2w%253D%253D/?\_l=en\_US](https://www.linkedin.com/pulse/gxfs-connect-2023-technology-community-decides-success-andreas-weiss%3FtrackingId=VvkM0xb3S9yUyHIHjQEX2w%253D%253D/?\_l=en\_US)


**Sovereign Cloud Stack Release 5**

   * 20 Sep 2023
   * [https://scs.community/release/2023/09/20/release5/](https://scs.community/release/2023/09/20/release5/)
   * CI testing of k8s platform, harbor integration, IPv6, new cluster stacks, ... 6 public providers
   * Hackathon Nov 8 in Dresden (with Cloud\&Heat / ALASCA)


**Members Monthly Updates Webinar**

   * 22 Sep 2023
   * 13:30 – 14:15
   * Microsoft Teams 
   * For Gaia-X members only
   * [https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/21](https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/21)


**Data Space Business Committee Event**

   * 23 Oct 2023
   * 11:00 – 13:00
   * [https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/24](https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/24)


**Gaia-X Roadshow** 

   * October 10, 6 - 11pm, Wolfsburg (Germany)
   * Registration: [https://www.eco.de/event/gaia-x-roadshow-wolfsburg](https://www.eco.de/event/gaia-x-roadshow-wolfsburg) 


**EuProGigant Open House Day**

   * October  10 - 11, Berlin (Germany)
   * EuProGigant ecosystem of five projects
   * Registration open, 250 PAX, physical participation only
   * [https://euprogigant-openhouseday2023.b2match.io/](https://euprogigant-openhouseday2023.b2match.io/)


**Future Congress Digital** 

   * October 11, Wolfsburg (Germany)
   * Gaia-X and Digital Mobility Ecosystems
   * [https://www.futurecongress.digital/anmeldung](https://www.futurecongress.digital/anmeldung)


**EclipseCon**

   * October 16 - 19, Ludwigsburg (Germany)
   * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
   * Workshop "Federation Services @ Eclipse" during the community day on Monday, October 16th 
       * [https://www.eclipsecon.org/2023/community-day](https://www.eclipsecon.org/2023/community-day)
   * Dedicated workshop "Federation Services" at the first day of the week. 


**GXFS Tech Workshop**

   * October 17, Reutlingen (Germany)
   * Registration and Agenda [https://www.eco.de/event/gxfs-tech-workshop-4/](https://www.eco.de/event/gxfs-tech-workshop-4/) 
   * Location: **INNOPORT Reutlingen** Max-Planck-Straße 68/1/ 72766 Reutlingen
   * Topics:
       * **„Was tragen Initiativen wie Gaia-X und Manufacturing-X zur Digitalisierung der Industrie bei?** - In German
       * Hands on with the WFE - In English 


**Gaia-X Summit 2023**

   * November 9-10, Alicante (Spain)
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


### **Credential Event Service (CES) Demonstration with example Service-Meister AutoTiM**

   * CES is available in two versions
       * [https://ces-v1.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/get\_credentials\_events](https://ces-v1.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/get\_credentials\_events)
       * [https://ces-development.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/get\_credentials\_events](https://ces-development.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/get\_credentials\_events)
       * Repo for Issues and Contributions: [https://gitlab.com/gaia-x/lab/credentials-events-service](https://gitlab.com/gaia-x/lab/credentials-events-service)
   * Example Service from Servicemeister
       * Service-Meister: [https://www.servicemeister.org/en/](https://www.servicemeister.org/en/)
       * DE: [https://www.gxfs.eu/de/pionierarbeit-fuer-die-zukunft-von-daten-und-ki-oekosystemen/](https://www.gxfs.eu/de/pionierarbeit-fuer-die-zukunft-von-daten-und-ki-oekosystemen/)
       * EN: [https://www.gxfs.eu/pioneering-the-future-of-data-and-ai-ecosystems/](https://www.gxfs.eu/pioneering-the-future-of-data-and-ai-ecosystems/)
   * Service AutoTiM application
       * Based on previously collected sensor data, various machine learning models are trained for supervised classification. 
       * [https://www.servicemeister.org/wp-content/uploads/2023/02/SM-M1-1-inovex-v2-EN-2.pdf](https://www.servicemeister.org/wp-content/uploads/2023/02/SM-M1-1-inovex-v2-EN-2.pdf)
       * Portal: [https://servicemeister.pontus-x.eu/asset/did:op:21a77aab5a4304da2c814e117b2d2f17a1e781c4b63e7200caae7bfc08c1d851](https://servicemeister.pontus-x.eu/asset/did:op:21a77aab5a4304da2c814e117b2d2f17a1e781c4b63e7200caae7bfc08c1d851)
       * Create Service Credential: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
       * Verify: [https://servicemeister.pontus-x.eu/verify?did=did%3Aop%3A21a77aab5a4304da2c814e117b2d2f17a1e781c4b63e7200caae7bfc08c1d851](https://servicemeister.pontus-x.eu/verify?did=did%3Aop%3A21a77aab5a4304da2c814e117b2d2f17a1e781c4b63e7200caae7bfc08c1d851)
       * Compliance Service: [https://compliance.gaia-x.eu/v1/docs#/credential-offer/CommonController\_issueVC](https://compliance.gaia-x.eu/v1/docs#/credential-offer/CommonController\_issueVC)
       * POST via CES
       * GET via CES
       * Resolve via Portal: 
           * [https://servicemeister.pontus-x.eu/verify](https://servicemeister.pontus-x.eu/verify)
           * [https://aquarius.v4.delta-dao.com/](https://aquarius.v4.delta-dao.com/)
       * Resolve via Terminal


curl --location --request GET '[https://aquarius.v4.delta-dao.com/api/aquarius/assets/metadata/did:op:21a77aab5a4304da2c814e117b2d2f17a1e781c4b63e7200caae7bfc08c1d851](https://aquarius.v4.delta-dao.com/api/aquarius/assets/metadata/did:op:21a77aab5a4304da2c814e117b2d2f17a1e781c4b63e7200caae7bfc08c1d851)'



curl -s "[https://aquarius.v4.delta-dao.com/api/aquarius/assets/metadata/did:op:21a77aab5a4304da2c814e117b2d2f17a1e781c4b63e7200caae7bfc08c1d851](https://aquarius.v4.delta-dao.com/api/aquarius/assets/metadata/did:op:21a77aab5a4304da2c814e117b2d2f17a1e781c4b63e7200caae7bfc08c1d851)" | python3 -c "import sys, json; print(json.load(sys.stdin)['additionalInformation']['gaiaXInformation']['serviceSD']['url'])"

Can be performed with all Gaia-X compliant service credentials, independend of ecosystems to facilitate a federated catalogue across ecosystems building on the Trust Framework.

Example from Gaia-X 4 Future Mobility: [https://portal.moveid.eu/asset/did:op:2e148b5c1ff12590c13bc018100743f0253f22f49cbaf7876612b2417f961789](https://portal.moveid.eu/asset/did:op:2e148b5c1ff12590c13bc018100743f0253f22f49cbaf7876612b2417f961789)

Example from COOPERANTS: [https://cooperants.pontus-x.eu/asset/did:op:6fe6ae5f546adf88606a0da158389f44a0801c3fe87ddb6e1f63e9120e07989c](https://cooperants.pontus-x.eu/asset/did:op:6fe6ae5f546adf88606a0da158389f44a0801c3fe87ddb6e1f63e9120e07989c)



### Updates from other lighthouses / projects / developers / lab?

   * No updates.
 

### AOB

   * **Gaia-X WG 5: Sustainable Open Mobility Services (SOMS)**
       * New project application in the Gaia-x 4 Future Mobility
       * Plan: Hand in proposal End of 2023
       * Implementation of real time Life Cycle Assessment to evaluate environmental impacts in Gaia-X 
       * Open Mobility Services evaluate vehicle operation based on Digital Twin data
       * Consortium 
           * contains: A Component and technology supplier, an LCA software developer, an interface programmer, the city of Hamburg, the DIN and two potential fleet owners
           * lacks: Eventually a fleet provder for implementing use cases, Software developer for the OMS
       * Important aspects for OMS / Interfaces: Open source and interoperability! (e.g. compatibility between different services)
       * Contact: [https://matrix.to/#/@nilez:matrix.org](https://matrix.to/#/@nilez:matrix.org)

   * **Announcement: Bi-weekly XFSC Catalogue Developers' Community Call**
      * Now fixed for Thu 13:00–14:00 Central European, starting from 28 September.
       * Here is the dial-in info: [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_YjhhODcwY2MtMWVmOS00ZmQzLTgyYjUtYzQyZWRmZjVjMTdm%40thread.v2/0?context=%7b%22Tid%22%3a%22f930300c-c97d-4019-be03-add650a171c4%22%2c%22Oid%22%3a%22dcd79abd-4e0c-4e77-be40-6615ea37f6fb%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_YjhhODcwY2MtMWVmOS00ZmQzLTgyYjUtYzQyZWRmZjVjMTdm%40thread.v2/0?context=%7b%22Tid%22%3a%22f930300c-c97d-4019-be03-add650a171c4%22%2c%22Oid%22%3a%22dcd79abd-4e0c-4e77-be40-6615ea37f6fb%22%7d)
       * Meeting ID: 358 368 137 698 
       * Passcode: a37xcu 
       * Please let Christoph Lange <christoph.lange-bever@fit.fraunhofer.de> me know your email address to receive a proper invite.













