MVG/Piloting Call: 2021-12-09

1. Interesting sessions/events/news

- SCS Community Summit (Eduard Itrich) 
-> You can register here: https://eventyay.com/e/cf85f6e7

2. Gaia-X Hackathon #2 results

Feedback:
- Amazing spirit of organizers and participants
- Way more results in all tracks than last Hackathon
- Collaboration between tracks was amazing
- Overall everyone is very happy with how the Hackathon went

Ideas for Improvement:
- Find tool for collaborative code review in real-time
-> Line by line code review was challenging in virtual meeting
- All tracks should participate in preperation and provide results after Hackathon as fast as possible (e.g., providing slides)
- Breaks in tracks were not aligned which made track hopping difficult
- Some tasks were the same as in Hackathon #1
-> Tasks will be identified and will not happen again

Next Hackathon (Hackathon #3):
- GXFS Hackathon will take place in March (week 12 or 13)
-> Scope: Implementation of Federation Services
-> Plan: Align this Hackathon with Hackathon #3
-> Hybrid event (digital and in person)
- Challenge: Two target groups for Hackathon
-> 1. Newcomers who never worked in Gaia-X before and who use the event to get onboarded
-> 2. Core group that is continuously building and deep down the rabbit hole
-> In Hackathon #2 in track 2: Easy tasks before lunch, more advanced tasks afterwards
--> Idea: Seperate tracks for beginners and advanced participants? Create visual roadmap for participants to navigate?
--> Due to GXFS: Probably a lot more newcomers in Hackathon #3

Statistics and Technical Results (Martin Pilka)
- You can find the presentation here: https://dl.dnation.cloud/dl/pub/Gaia-X-Hackathon_2-Results.pdf

3. Towards a plan for 2022 & role of OWP MVG

- Not settled yet
- Goal is to follow what we started: Spread knowledge in the community, support and push forward the roadmap of Gaia-X
- Overall idea apart from integrating different things and aligning them with Federation Services: Continue the story that we started (ACME example as presented in previous sessions)
-> Have discussions on mailing list on alignment and derive concrete steps

4. AOB 

[nothing]