# Meeting notes Gaia-X OSS community July 13, 2023

## Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   6. How to export Credentials from Wallet - Ideas and Opinions
   7. AOB


##  Focus of the weekly

   * Updates from the community


## General Notes

   * **Participants in the call:** 31
   * **Acceptance of Agenda:** Accepted by the audience.
   * **Acceptance of last week's minutes:** Accepted by the audience.
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-07-06.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-07-06.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Arthur Sore, Gaia-X AISBL
   * Enrico La Vela, Aruba
   * Prateek, SmartSense Solutions


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Compliance Service Swagger UI [https://compliance.lab.gaia-x.eu/v1/docs/](https://compliance.lab.gaia-x.eu/v1/docs/)
   * Gaia-X Registry Swagger UI: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Gaia-X Registry: [https://registry.lab.gaia-x.eu/](https://registry.lab.gaia-x.eu/)
   * Gaia-X Registration Number Service: [https://registrationnumber.notary.gaia-x.eu/v1/docs/](https://registrationnumber.notary.gaia-x.eu/v1/docs/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**GXFS Tenders launched.**

   * Identity \& Trust marks the start.
   * [https://www.gxfs.eu/de/ausschreibung-identity-trust/](https://www.gxfs.eu/de/ausschreibung-identity-trust/)


**GXFS CONNECT 2023**

   * September 5-6, 2023, Berlin
   * [https://www.gxfs.eu/gxfs-connect-2023/](https://www.gxfs.eu/gxfs-connect-2023/)
   * Max. PAX 250
   * Agenda in the making


**Next GXFS Workshops (on site)**

   * 5 \& 6 September, Berlin, parallel to GXFS Connect 2023, also in Villa Elisabeth
   * 25 \& 26 October, near Stuttgart
   * Late November/Early December, Cologne 


**SODA DATA Vision 2023**

   * September 18, Bilbao
   * [https://www.sodafoundation.io/events/sodadatavision2023/](https://www.sodafoundation.io/events/sodadatavision2023/)
   * Topics (among others):
       * Data Catalog for Files and Object
       * Cloud Native Data Management
       * Hybrid Multi-Cloud Data Lifecycle \& Protection


**Future Congress Digital** 

   * October 11, Wolfsburg (Germany)
   * Gaia-X and Digital Mobility Ecosystems
   * [https://www.futurecongress.digital/anmeldung](https://www.futurecongress.digital/anmeldung)


**EclipseCon**

   * October 16 - 19, Ludwigsburg (Germany)
   * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
   * Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects


**Gaia-X Summit 2023**

   * November 9-10, 2023
   * Alicante, Spain
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


**GXFS-DE / Eclipse XFSC transition to Eclipse (ongoing)**

   * Code transition completed
   * You can follow the old links and readme docs to the new location
   * You are invited to contribute, don't forget to register with  Eclipse. For contributor rights you need show you are knowledgeable. Once proven, Developer rights can be granted.
   * Date for the first release soon and webinar will be scheduled as well, GXFS team will keep everybody updated
   * You can use [https://accounts.eclipse.org/mailing-list/xfsc-dev](https://accounts.eclipse.org/mailing-list/xfsc-dev) mailing list to get in touch with the team


### Updates from other lighthouses / projects / developers / lab?

   * Following the already established 22.10 Trust Framework the development is following.
   * Lab: Compliance with mandatory LegalRegistrationNumber VC issued by notary on development environment. Examples provided in the swagger to see what the VP looks like ([https://compliance.lab.gaia-x.eu/development/docs/#/credential-offer/CommonController\_issueVC)](https://compliance.lab.gaia-x.eu/development/docs/#/credential-offer/CommonController\_issueVC)). 
   * Simplified flow chart in compliance doc of an onboarding: [https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/README-api.md#simplified-application-flow](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/README-api.md#simplified-application-flow)
   * Work in progress on the Wizard to be able to issue all VCs fluently. 
   * Merge request for mandatory GaiaXTermsAndConditions VC ready.


### How to export Credentials from Wallet - Ideas and Opinions

   * Exporting Credentials applies mostly for Participant VC; Service VCs will be in catalogues and mostly public (depending on the ecosystem) so they don't need to be stored in a wallet
   * Using a simple JSON file to import/export the participant VC on a local storage
   * Discussion on difference between importing/exporting from a wallet to a local storage, vs from a wallet to another wallet
   * Discussion on how the private keys are involved in the import/export of VC
   * [https://docs.walt.id/v/ssikit/concepts/oidc/presentation-exchange](https://docs.walt.id/v/ssikit/concepts/oidc/presentation-exchange)


### AOB

   * [...]





