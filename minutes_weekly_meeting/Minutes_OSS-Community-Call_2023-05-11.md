# Meeting notes Gaia-X OSS community May 11, 2023

## Agenda

1. Competition \& Antitrust Guidelines (1 min, Kai)

2. Acceptance of last week meeting notes and today's Agenda (1 min, Kai)

3. Introduction of new participants and regular contributors (1-10 mins, Kai \& newcomers)

4. Interesting sessions, events and news (10 mins, all)

5 Tech event \& Hackathon #6 (10  mins, Pierre)

6. Updates from other lighthouses / projects / developers (all) 

7. AOB (all)



## Focus of the weekly

   * Updates from the community
   * Tech-X and Hackathon No. 6


## General Notes

**Participants in the call:** 22

**Acceptance of Agenda:** Accepted by the audience.

**Acceptance of last week's minutes:** Accepted by the audience.

**Link to last week's minutes:**[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-04-27.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-04-27.md)

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * No new participants.


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Compliance Service [https://compliance.lab.gaia-x.eu/v1/docs/#/credential-offer/CommonController\_issueVC](https://compliance.lab.gaia-x.eu/v1/docs/#/credential-offer/CommonController\_issueVC)
   * Registry: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)


### Interesting sessions, events and news

   * **Cloud Expo Europe, Frankfurt, Germany**
       * May 10-11, Frankfurt
       * [https://www.cloudexpoeurope.de/themes2023](https://www.cloudexpoeurope.de/themes2023)
       * Gaia-X at the heart of the Expo, Gaia-X Arena 
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de


   * **Austrian Gaia-X Meetup**
       * May 16 (18:00-20:00)
       * [https://www.linkedin.com/posts/gaia-x-hub-austria\_vienna-gaiaxat-gaiaxmeetup-activity-7062082187117879296-6CHA](https://www.linkedin.com/posts/gaia-x-hub-austria\_vienna-gaiaxat-gaiaxmeetup-activity-7062082187117879296-6CHA)
       * Location
           * Verein Industrie 4.0 Österreich
           * Mariahilfer Straße 37-39, 1060 Wien
           * "Graphen" room, 5th floor
       * Informal get-together: talk, followed by networking over food \& drinks
       * Talk held in English


   * **SCS Summit, Berlin, Germany**
       * Berlin, May 23+24
       * [https://scs.community/summit](https://scs.community/summit)
       * German and English language content


   * **MyData conference**
       * May 31 - June 01, Helsinki
       * [https://www.mydata.org/event/mydata-2023/](https://www.mydata.org/event/mydata-2023/)


   * **EclipseCon, Ludwigsburg, Germany**
       * October 16 - 19, 2023
       * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
       * Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects
       * **CFP** is currently open until **June 16**. Early submission deadline is **June 2** to have a better chance to have your proposition accepted.


### Tech-X \& Hackathon No. 6

   * **Tech-X \& Hackathon #6 on site event**
       * [https://gaia-x.eu/tech-x/](https://gaia-x.eu/tech-x/)


   * **Report (Pierre)**
       * Roughly 300 people, half of them not members yet
       * High interest in Gaia-X
       * [...]


   * Video about **technical update** will come soon - link will be shared via mailing list


   * Link to backlog: [https://gaia-x.atlassian.net/jira/software/c/projects/TAG/boards/43/backlog?issueLimit=100](https://gaia-x.atlassian.net/jira/software/c/projects/TAG/boards/43/backlog?issueLimit=100)


### Updates from other lighthouses / projects / developers?

   * No updates.
### 

### AOB

**Task for the Community**

**Express your expectations to the OSS-Community meeting (via mailing list, Slack/Matrix, here in the pad)**

   * What information are you looking for?
   * What do you expect?
   * How can we make this more useful?