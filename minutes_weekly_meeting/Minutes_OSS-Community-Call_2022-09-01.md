# Meeting notes Gaia-X OSS community September 1, 2022



## Focus of the weekly

- Hackathon No. 5



## Proposed agenda



   * Competition \& Antitrust Guidelines
   * Acceptance of Agenda
   * Introduction of new participants and regular contributors 
   * Interesting sessions, events and news
   * Organizational topics
       * Next week there will be no meeting
   * Work on the deliverables
       * Organization of the Gaia-X Hackathon
       * Demonstrations
       * Release of Gaia-X Open-Source Software
   * AOB


## General notes

   * **Participants in the call:** 13
   * **Agenda:** Accepted, no points were added.


### Interesting sessions, events and news



Upcoming events: Please see [https://gaia-x.eu/events/](https://gaia-x.eu/events/) and last weeks meeting notes.



   * Gaia-X Trust Framework 22.06. expected to be released to production within the next two weeks. See [https://gitlab.com/gaia-x/lab/compliance](https://gitlab.com/gaia-x/lab/compliance)
   * New video series "interviews with the GXFS work package leads": [https://youtube.com/playlist?list=PLx9hLnO5yRKDi\_6NQ9X\_MclZTSjgt4EcT](https://youtube.com/playlist?list=PLx9hLnO5yRKDi\_6NQ9X\_MclZTSjgt4EcT)
   * GXFS Summit 2022 upcoming, so there will be no meeting next week.
   * Meet Up: Rebooting the Web of Trust (Berlin, 12th Sep.) [https://www.meetup.com/de-DE/rebooting-web-of-trust-berlin/events/288073264/](https://www.meetup.com/de-DE/rebooting-web-of-trust-berlin/events/288073264/)


### Hackathon



**Find proposals here:** [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/Hackathon-5-Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/Hackathon-5-Proposals)

   * Several new proposals were added:
       * 01 MoveID - Integration of a Mobility Data Ecosystem into the Gaia-X Framework
       * 02 Privacy-preserving Gaia-X Edge to Cloud integration
       * 03 Self-Descriptions
       * 04 GXFS
       * 05 Gaia-X Trust Framework 22.06
   * SCS proposal will follow
       * Focus: User Federation and OpenID


**Call for proposals - drop your ideas!** Will not necessarily make it to the agenda, but let's collect ideas! Also, you do *not* have to be responsible for the proposal you add. :)



**Call for organizers!** We need some helping hands - if you'd like to volunteer to support the organization of a track, please reach out and we'll add you to the organizer meetings. Your support is much appreciated!


