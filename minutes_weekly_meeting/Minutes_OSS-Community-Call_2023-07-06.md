# Meeting notes Gaia-X OSS community July 06, 2023

## Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   6. AOB


##  Focus of the weekly

   * Updates from the community

## General Notes

   * **Participants in the call:** 25
   * **Acceptance of Agenda:** Accepted by the audience.
   * **Acceptance of last week's minutes:** Accepted by the audience.
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-06-29.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-06-29.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * No new participants in the call.

**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Compliance Service Swagger UI [https://compliance.lab.gaia-x.eu/v1/docs/](https://compliance.lab.gaia-x.eu/v1/docs/)
   * Gaia-X Registry Swagger UI: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Gaia-X Registry: [https://registry.lab.gaia-x.eu/](https://registry.lab.gaia-x.eu/)
   * Gaia-X Registration Number Service: [https://registrationnumber.notary.gaia-x.eu/v1/docs/](https://registrationnumber.notary.gaia-x.eu/v1/docs/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**IPCEI-CIS**

   * Call open for proposals until July 21, 2023
   * [https://www.gxfs.eu/de/foerderaufruf-ipcei-cis/](https://www.gxfs.eu/de/foerderaufruf-ipcei-cis/)


**GXFS Tenders launched.**

   * Identity \& Trust marks the start.
   * [https://www.gxfs.eu/de/ausschreibung-identity-trust/](https://www.gxfs.eu/de/ausschreibung-identity-trust/)


**GXFS CONNECT 2023**

   * 05-06 September 2023, Berlin


**Next GXFS Workshops (on site)**

   * 5 \& 6 September, Berlin
   * 25 \& 26 October, near Stuttgart
   * Late November/Early December, Cologne 


**SODA DATA Vision 2023**

   * September 18, Bilbao
   * Topics:
       * Data Catalog for Files and Object
       * Cloud Native Data Management
       * Hybrid Multi-Cloud Data Lifecycle \& Protection


**Future Congress Digital** 

   * October 11, Wolfsburg (Germany)
   * Gaia-X and Digital Mobility Ecosystems
   * [https://www.futurecongress.digital/anmeldung](https://www.futurecongress.digital/anmeldung)


**EclipseCon**

   * October 16 - 19, Ludwigsburg (Germany)
   * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
   * Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects
   * CFP is closed.


**Gaia-X Summit 2023**

   * November 9-10, 2023
   * Alicante, Spain
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


### Updates from other lighthouses / projects / developers / lab?

   * Gaia-X Trust Framework
       * Changes to institution identifier issuance process, as the legalRegistrationNumber a participant provides should be issued by a trusted notary
       * When released, on the GXDCH, this gx:legalRegistrationNumber will need to be issued from one of the trusted notaries, eg, the ones on the clearing houses. List of trusted issuers is there: [https://registry.lab.gaia-x.eu/v1/api/trusted-issuers/registration-notary]([]https://registry.lab.gaia-x.eu/v1/api/trusted-issuers/registration-notary[])
       * Changes to Terms \& Conditions as the issuer of a participant is supposed to agree to GaiaX terms and conditions.
       * Gaia-X AISBL will enforce that by asking you to provide a VC representing this T\&Cs agreement
       *  Wizard will be updated too to ease this transition. 
       * Everybody is invited to use the new guided stepper in the wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)


### AOB

   * 9 of the GXFS components have been migrated. The rest 5 will be migrated until 14th of July
   * New location of the repos [https://gitlab.eclipse.org/eclipse/xfsc/](https://gitlab.eclipse.org/eclipse/xfsc/)
   * The projects are still available at their old location, but under the "Archived projects" tab
   * Don't forget to create an Eclipse Account if you are willing to contribute. 


#### **Task for the Community**

**Express your expectations to the OSS-Community meeting (via mailing list, Slack/Matrix, here in the pad)**



   * What information are you looking for?
   * What do you expect?
   * How can we make this more useful?



