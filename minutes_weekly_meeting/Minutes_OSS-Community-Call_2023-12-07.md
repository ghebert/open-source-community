## Meeting notes Gaia-X OSS community December 07, 2023

### Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   6. Presentation of the use of wallet in Gaia-X Ecosystem by GXFS-FR team
   7. AOB

### Focus of the weekly

   * Updates from the community


### General Notes

   * **Participants in the call:** 45
   * **Acceptance of last week's minutes:**  Yes
   * **Acceptance of the agenda:** Yes
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-11-23.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-11-23.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * No new participants in the call.


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**Latest Gaia-X Publications**

   * [https://gaia-x.eu/media/publications/](https://gaia-x.eu/media/publications/)


**News in Contracts and Ontologies**

   * [https://gaia-x.eu/news-press/latest-news/](https://gaia-x.eu/news-press/latest-news/)


**Gaia-X Summit videos are on youtube**

   * [https://www.youtube.com/@gaia-x5188/videos](https://www.youtube.com/@gaia-x5188/videos)


**Data Space Protocol Update**

   * IDSA Tech Talk | Unveiling the Dataspace Protocol 
   * [https://register.gotowebinar.com/register/4601736972768854103](https://register.gotowebinar.com/register/4601736972768854103)


**Gaia-X Workshop Luxembourg (only for Gaia-X Hubs, Ecosystems, Lighthouse projects)**

   * December 5-6 (maybe also 4) - Note, this is not an open event
   * Location: Luxembourg
   * Register: [https://events.gaia-x.lu/data-summit-luxembourg-schengen-x](https://events.gaia-x.lu/data-summit-luxembourg-schengen-x)
   * Info: [https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh](https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh)


**GXFS Tech Workshop**

   * December 12-13, Cologne (Germany)
   * Location: 25hours Hotel Koeln The Circle
   * Registration [https://www.gxfs.eu/gxfs-tech-workshop-5/](https://www.gxfs.eu/gxfs-tech-workshop-5/) 
   * Download Agenda here : [https://www.gxfs.eu/gxfs-tech-workshop-5/](https://www.gxfs.eu/gxfs-tech-workshop-5/)
   * On site event - no remote participation 
       * Exercise 1 (draft)
       * Here we will guide our attendees to do the following on their systems:
           * Deploy all the applications of Smart-X POC 
           * Configure the relevant XFSC components like OCM and PCM
           * Create Legal participant
           * Create Resource
           * Create Service Offering
           * Check the service offering published to CES
       * Exercise 2 (draft)
       * Here we will guide our attendees to do the following on their systems:
           * Deploy the catalogue frontend and backend
           * Setup the database
           * Configure the CES API
           * Fetch the service offerings from CES and see in their UI


**GXFS Tech Workshop #6**

   * **22 and 23 January 2024 in Frankfurt.**
   * Agenda coming soon. Focus on TRAIN and DNS Sec
   * Registration: [https://www.gxfs.eu/gxfs-tech-workshop-6/](https://www.gxfs.eu/gxfs-tech-workshop-6/) 


### Updates from other lighthouses / projects / developers / lab

   * No updates.


###  Use of wallet in Gaia-X Ecosystem GXFS-FR 

   * Presentation and demonstration of the use of wallet in Aster-X ecosystem by GXFS-FR team
       * Discussion around jsonld proofs, sd-jwt, vc-jwt and potential deprecation of jsonld proofs: ICAM has to work on it for Gaia-X
       * Link of the presentation (including videos) : [https://drive.google.com/file/d/1T9j75zgDc7dKIAD8no9kKpH1O23i4EJU/view?usp=sharing](https://drive.google.com/file/d/1T9j75zgDc7dKIAD8no9kKpH1O23i4EJU/view?usp=sharing)


### AOB

   * Nothing.
