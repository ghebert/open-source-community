# Meeting notes Gaia-X OSS community February 2, 2023

## Focus of the weekly

   * Hackathon No. 6


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Tech event \& Hackathon #6

6. AOB



## General Notes

**Participants in the call:** 30

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-01-26.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-01-26.md)

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * Paolo Vecchi (Omnis Cloud, Eclipse Foundation)


**Useful resources for newcomers:**

   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/) \& [https://docs.gaia-x.eu/framework/beta/](https://docs.gaia-x.eu/framework/beta/)
   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)




### Interesting sessions, events and news

   * **Gaia-X Hub Germany - Participant Onboarding and Self-Description Workshop**
       * February 3, 09:00 to 12:00 CET
       * Organized by acatech and deltaDAO
       * Will be held in English due to the huge interest from the international community
       * Creation of Gaia-X compliant participant Self-Descriptions to reach the 50% adoption goal of 2023 and to prepare projects for the Gaia-X Clearing Houses and work with Self-Descriptions
       * Includes Onboarding for the Gaia-X Web3 ecosystem


   * **Open Forum Europe**
       * February 3, Brussels
       * [https://openforumeurope.org/](https://openforumeurope.org/)


   * **FOSDEM**
       * February 4-5, Brussels
       * [https://fosdem.org/2023/schedule/](https://fosdem.org/2023/schedule/)
       * Sovereign Cloud devroom: [https://fosdem.org/2023/schedule/track/sovereign\_cloud/](https://fosdem.org/2023/schedule/track/sovereign\_cloud/)


   * **Manufacturing Data Space Event (virtual)**
       * February 6, 10:00 to 12:00 CET, online
       * Register here: [https://share-eu1.hsforms.com/1Jx3F3Pt\_TYKnHNEFamUiTQfjelv](https://share-eu1.hsforms.com/1Jx3F3Pt\_TYKnHNEFamUiTQfjelv)
       * Information: [https://gaia-x.eu/event/gaia-x-manufacturing-data-space-event/](https://gaia-x.eu/event/gaia-x-manufacturing-data-space-event/)


   * **Gaia-X Roadshow Hamburg**
       * February 9, Hamburg
       * Organized by eco
       * [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)
       * [https://www.eco.de/event/gaia-x-roadshow-powered-by-eco-hamburg/#tickets](https://www.eco.de/event/gaia-x-roadshow-powered-by-eco-hamburg/#tickets)
       * Restaurant Vlet in der Speicherstadt


   * **FOSS Backstage 23**
       * March 14-15, Berlin (and online)
       * Theme: *Community, Management \& Compliance in Open Source Development*
       * [https://23.foss-backstage.de/](https://23.foss-backstage.de/)
       * Eduard \& Ariane will be there to present **Community2**
           * Community2 is our new community for Open Source Community Managers
           * More information: [https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop](https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop)


   * **Market-X, Vienna, Gaia-X Market Adoption Event**
       * March 14-15, Vienna, Aula der Wissenschaften, [https://www.aula-wien.at/](https://www.aula-wien.at/) 
       * Updated website: [https://gaia-x.eu/market-x/](https://gaia-x.eu/market-x/)
       * Market-X is a great opportunity to present projects, find business matches, connect with our lighthouse projects and other initiatives, start building cross-industry collaborations, learn about Gaia-X value chains on different levels and become part of Gaia-X market adoption community.
       * The event will feature a large exhibition where you can network and connect with the community, as well as different booth options to showcase hubs, projects, verticals, and get visibility within the Gaia-X community. Learn about the booth options at  [https://gaia-x.eu/market-x/](https://gaia-x.eu/market-x/)


   * **Data Spaces Symposium \& Deep-Dive Day**
       * March 21–23, The Hague
       * Info and registration link: [https://internationaldataspaces.org/data-spaces-symposium]([]https://internationaldataspaces.org/data-spaces-symposium[])


   * **Hannover Messe, Hannover, Germany**
       * April 17-21, Hanover (five days is new in 23)
       * [https://www.hannovermesse.de/en/](https://www.hannovermesse.de/en/)
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de
       * Gaia-X Area


   * **Cloud Expo Europe, Frankfurt, Germany**
       * May 10-11, Frankfurt
       * [https://www.cloudexpoeurope.de/themes2023](https://www.cloudexpoeurope.de/themes2023)
       * Gaia-X at the heart of the Expo, Gaia-X Arena 
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de


   * **MyData conference**
       * May 31 - June 01, Helsinki
       * [https://www.mydata.org/event/mydata-2023/](https://www.mydata.org/event/mydata-2023/)


   * **New event series: GXFS Tech Workshops**
       * There will be a series of GXFS Tech Workshops hosted over the year
           * Two days hands-on workshops, organized by eco
           * Working on several use cases and concrete implementations
       * Around March or April, most likely hosted in Cologne
           * Livestream also planned for passive participation


### Tech event \& Hackathon No. 6

   * **Infos and Guideline**
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home)


   * **Location and date**
       * Azkuna Zentroa in Bilbao, Spain on the 3\&4th of May 2023


   * **Discussion on making it a hybrid event**
       * Suggestion: Make some tracks/sessions solely on-site but still offer online tracks in addition to that
       * Challenge: We'd need moderators for both, online and on-site


   * **Registration**
       * Will be open in a few weeks


   * **Call for proposals**
       * If you have anything in mind for the Hackathon and/or the tech event, please add to the proposals page (or align on suggestions that have already been added, preferably via the mailing list) [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals)
       * Deadline for proposals: March 7, 2023


   * **Call for organizers**
       * If you would like to help organizing the Hackathon, please reach out on the mailing list or to Cristina!




### AOB

   * Significant progress on Gaia-X Compliance (Pierre)
       * [https://gitlab.com/gaia-x/lab/compliance](https://gitlab.com/gaia-x/lab/compliance)
       * [https://gaia-x.atlassian.net/jira/software/c/projects/TAG/issues](https://gaia-x.atlassian.net/jira/software/c/projects/TAG/issues)
           * Looking for support here!





