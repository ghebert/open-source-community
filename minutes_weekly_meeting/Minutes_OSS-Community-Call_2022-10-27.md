# Meeting notes Gaia-X OSS community October 27, 2022

## Focus of the weekly

   * Demo of the Gaia-X Trust Framework Compliance Service


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Compliance Service - How to create and validate Self Descriptions? 

6. AOB



## General Notes

**Participants in the call:** 16

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to the minutes:** [https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * Eric Samson (Microsoft)
   * Henrich C. Pöhls (Huawei)


**Useful resources for newcomers:** The Gaia-X Framework. [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)



### Interesting sessions, events and news



   * **Survey "Acceptance of Gaia-X"**
       * Call for participants!
       * Open for 3 weeks, will take you approx. 10 minutes
       * In German, but you can simply translate the page and participate that way too :)
       * Link: [https://survey.lamapoll.de/Akzeptanz\_Gaia-X/](https://survey.lamapoll.de/Akzeptanz\_Gaia-X/)


   * **Gaia-X Roadshow powered by eco**
       * Oct 20 in Munich, Nov 30 in Frankfurt, date for Hamburg will follow soon, Viena-Austria 16.03.2022, 
       * Link: [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)


   * **Cloud Expo Europe Madrid**
       * Oct 26-27 in Madrid. Participation of CEO and Chair of Governmental Advisory Board of Gaia-X AISBL in the panel discussion panel "Gaia-X: The next wave of European digital innovation".
       * LinK: [https://www.cloudexpoeurope.es/](https://www.cloudexpoeurope.es/) || [https://www.cloudexpoeurope.es/programa-de-conferencias-2022/panel-gaia-x](https://www.cloudexpoeurope.es/programa-de-conferencias-2022/panel-gaia-x)


   * **GC Tech-Talk No. 5 (Cloud\&Heat)**
       * Oct 27 (Thu) 2 - 3pm (online)
       * "Share without Sharing - Swarm Learning to analyze any private data from any data owner without sharing data to foster digital sovereignty" (HPE) and "Basic digital service - a utility perspective" (EnBW)
       * Link: [https://mautic.cloudandheat.com/techtalk-registration](https://mautic.cloudandheat.com/techtalk-registration)
       * Or just join: [https://us02web.zoom.us/j/89142262107?pwd=d0VXa05TQXpEa3crSFhHSk04ZWt0Zz09](https://us02web.zoom.us/j/89142262107?pwd=d0VXa05TQXpEa3crSFhHSk04ZWt0Zz09) :)


   * **Open Source Experience**
       * Nov 8 - 9
       * Link: [https://www.opensource-experience.com/en/](https://www.opensource-experience.com/en/)


   * **Gaia-X Summit 2022**
       * Nov 17 - 18, Online Attendance (unlimited), Physical Attendance (very limited)
       * Link: [https://gaia-x.eu/event/gaia-x-summit-2022/](https://gaia-x.eu/event/gaia-x-summit-2022/)


   * **Inside Gaia-X: Self Sovereign Identity - what is behind it?**
       * Oct 27, 10:30 am - 12:00 pm
       * Event organized by Austrian Gaia-X Hub
       * Kai is giving a talk!
       * Link: [https://www.gaia-x.at/en/event/inside-gaia-x-4-self-sovereign-identity-what-is-behind-it/](https://www.gaia-x.at/en/event/inside-gaia-x-4-self-sovereign-identity-what-is-behind-it/)
       * Join here: [https://us02web.zoom.us/j/83958625751?pwd=WlVmK1pOTCthdHZkb2VHZW1MWFBzQT09](https://us02web.zoom.us/j/83958625751?pwd=WlVmK1pOTCthdHZkb2VHZW1MWFBzQT09)


### Compliance Service - How to create and validate Self Descriptions? 

   * Compliance Service: [https://compliance.gaia-x.eu/](https://compliance.gaia-x.eu/)
   * Gaia-X Trust Framework: [https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)
   * Gaia-X Framework Knowledge Base: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Deployment Scenarios: [https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/](https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/) 


### AOB

   * [https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/changelog/#2022-october-2210](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/changelog/#2022-october-2210)

