## Meeting notes Gaia-X OSS community October 12, 2023

### Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   6. AOB

###  Focus of the weekly

   * Updates from the community.


### General Notes

   * **Participants in the call:** 43
   * **Acceptance of last week's minutes:** Yes.
   * **Acceptance of the agenda:** Yes.
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-10-05.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-10-05.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Freddy Guigma, Software AG
   * Sofia Ananieva, ABB Corporate Research Center Germany
   * Stuart J Mackintosh - OpenDigital


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: https://list.gaia-x.eu/postorius/lists/
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**Workshop "Gaia-X Federation Services @ Eclipse"**

   * Workshop "**Gaia-X Federation Services @ Eclipse**" during the EclipseCon community day on Monday, October 16th 
       * [https://www.eclipsecon.org/2023/gaia-x-federation-services-eclipse](https://www.eclipsecon.org/2023/gaia-x-federation-services-eclipse)
   * EclipseCon October 16 - 19, Ludwigsburg (Germany)
       * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)


**GXFS Tech Workshop**

   * October 17, Reutlingen (Germany)
   * Registration and Agenda [https://www.eco.de/event/gxfs-tech-workshop-4/](https://www.eco.de/event/gxfs-tech-workshop-4/) 
   * Location: **INNOPORT Reutlingen** Max-Planck-Straße 68/1/ 72766 Reutlingen
   * Topics:
       * **„Was tragen Initiativen wie Gaia-X und Manufacturing-X zur Digitalisierung der Industrie bei?"** - In German 
       * Hands on with the WFE - In English 


**SCS Hackathon**

   * Nov 8, Dresden (Germany)
   * More info soon!


**Maybe: Open Source Meetup organized by the Eclipse Foundation**

   * November 8 - 16:00-19:00, Alicante (Spain)
   * Anybody interested? Ping gael.blondelle@eclipse-foundation.org before Friday Oct 20th.


**Gaia-X Summit 2023**

   * November 9-10, Alicante (Spain)
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


**Gaia-X Workshop Luxembourg (only for Gaia-X Hubs, Ecosystems, Lighthouse projects)**

   * December 5-6 (maybe also 4)
   * Location: Luxembourg
   * Register: [https://events.gaia-x.lu/data-summit-luxembourg-schengen-x](https://events.gaia-x.lu/data-summit-luxembourg-schengen-x)
   * Info: [https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh](https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh)


**GXFS Tech Workshop**

   * December 12-13, Cologne (Germany)
   * Location: 25hours Hotel Koeln The Circle
   * Registration [https://www.gxfs.eu/gxfs-tech-workshop-5/](https://www.gxfs.eu/gxfs-tech-workshop-5/) 
   * Agenda coming 


### Updates from other lighthouses / projects / developers / lab?

XFSC technology upgrade:

   * New enhancement are scheduled and assigned to working parties. Key targets to be fully aligned with technology decision of the Gaia-X Trust Framework
       * [https://www.gxfs.eu/second-phase-of-the-xfsc-specification/](https://www.gxfs.eu/second-phase-of-the-xfsc-specification/)
       * TRAIN is planned for QA by End of 2023
       * The rest is agreed for End of Feb 2024 (PCM and PCM Cloud, OCM, TSA, NOT)
   * All the work will be done in the Eclipse workspace 
New SSI Whitepaper about DIDs released:

   * Link: https://www.gxfs.eu/did-whitepaper/ (Baseline Q4/2022)
   * Due to the agile development we are currently conducting an expert peer review and a new version in Q4/2023


**Gaia-X Lab updates:**

   * Lab: Resources, ServiceInstance, ServiceAccessPoint shapes/classes available on development : [https://compliance.lab.gaia-x.eu/development/docs/](https://compliance.lab.gaia-x.eu/development/docs/)
   * Lab: Small batch of fixes regarding signature/normalization following small incident on w3.org (notary, wizard)
 

### AOB

   * Nothing.



