# Meeting notes Gaia-X OSS community October 13, 2022


## Focus of the weekly

   * Organizational issues


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Call for building the agenda collaboratively

6. AOB



## General Notes

**Participants in the call:** 18

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to the minutes:** [https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * No new members.


**Useful resources for newcomers:** The Gaia-X Framework. [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)



### Interesting sessions, events and news



   * **Survey "Acceptance of Gaia-X"**
       * Call for participants!
       * Open for 3 weeks, will take you approx. 10 minutes
       * In German, but you can simply translate the page and participate that way too :)
       * Link: [https://survey.lamapoll.de/Akzeptanz\_Gaia-X/](https://survey.lamapoll.de/Akzeptanz\_Gaia-X/)


   * **Gaia-X Roadshow powered by eco**
       * Oct 20 in Munich, Nov 30 in Frankfurt, date for Hamburg will follow soon, Viena-Austria 16.03.2022, 
       * Link: [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)


   * **GC Tech-Talk No. 5 (Cloud\&Heat)**
       * Oct 27 (Thu) 2 - 3pm (online)
       * "Share without Sharing - Swarm Learning to analyze any private data from any data owner without sharing data to foster digital sovereignty" (HPE) and "Basic digital service - a utility perspective" (EnBW)
       * Link: [https://mautic.cloudandheat.com/techtalk-registration](https://mautic.cloudandheat.com/techtalk-registration)


   * **Open Source Experience**
       * Nov 8 - 9
       * Link: [https://www.opensource-experience.com/en/](https://www.opensource-experience.com/en/)


   * **Gaia-X Summit 2022**
       * Nov 17 - 18, Online Attendance (unlimited), Physical Attendance (very limited)
       * Link: [https://gaia-x.eu/event/gaia-x-summit-2022/](https://gaia-x.eu/event/gaia-x-summit-2022/)


### Call for Building the Agenda Collaboratively

   * If you would like to present a demo or discuss a certain topic, please add it in the agenda for the upcoming week, with your name and a time estimate
       * Topics should be connected with Gaia-X
       * Ideas for next week's agenda:
           * Dataspace vision: [https://aka.ms/dataspace-vision](https://aka.ms/dataspace-vision) (Matthias)
   * Some topics will be repeated in the calls - simply check the agenda before the call and decide if you want to join
   * Always feel free to ask questions - no stupid questions. Add to the pad, ask on the mailing list. We're happy to help!


### Gaia-X vs. Dataspaces Discussion

   * 22.09 architecture document does address this 
   * G-X does have a generic notion of data exchange services (as part of GXFS)
   * G-X does not have data connectors ... (nor middleware like databases)
   * G-X really focuses on the trust framework (compliance, self-descriptions, ...) to enable (trustable) federations
   * There is ongoing work within DSBA to align the architectures (97 pages)

