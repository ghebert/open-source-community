# Notes Gaia-X Open-Source Software Community Weekly, September 14, 2022

## Focus of the weekly

Hackathon No. 5



## Proposed Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of Agenda

3. Introduction of new participants and regular contributors 

4. Interesting sessions, events and news

   * 1. GXFS Connect 2022
   * 2. GXFS Roadshow Germany 2022-2023
   * 3. Gaia-X Summit 2022
5. Organizational topics

6. Work on the Deliverables

   * 1. Release of Gaia-X Open-Source Software - GXFS
   * 2. Organization of the Gaia-X Hackathon
7. AOB 



## General Notes

Participants in the call: 19

Agenda: Agenda accepted, no points added.



### Interesting sessions, events and news

Upcoming events: Please see [https://gaia-x.eu/events/](https://gaia-x.eu/events/) and last weeks meeting notes.



- GXFS Connect 2022 Berlin:

   * Summary of GXFS Connect [https://www.gxfs.eu/de/rueckblick-gxfs-connect/](https://www.gxfs.eu/de/rueckblick-gxfs-connect/)  
   * Follow-up Report : [https://www.gxfs.eu/de/nachbericht-gxfs-connect-2022/](https://www.gxfs.eu/de/nachbericht-gxfs-connect-2022/)
   * GXFS Study [https://www.gxfs.eu/de/studie-gxfs/](https://www.gxfs.eu/de/studie-gxfs/)

- GXFS Roadshow: [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)
- Gaia-X Summit 2022: [https://gaia-x.eu/event/gaia-x-summit-2022/](https://gaia-x.eu/event/gaia-x-summit-2022/
- Gaia-X Podcast: [https://gaia-x.eu/mediatech/gaia-x-podcasts/gaia-x-strategy-episode-2/](https://gaia-x.eu/mediatech/gaia-x-podcasts/gaia-x-strategy-episode-2/)
- Gaia-X Podcast: [https://www.youtube.com/watch?v=VOrucRQM61I](https://www.youtube.com/watch?v=VOrucRQM61I)


### Organizational topics

**Hackathon Organizer meeting** every week after the OSS community meeting. If you would like to be in the driver seat and support the event, please let us know.



### Work on Deliverables

#### GXFS Release

   * New Gaia-X Framework overview: [https://docs.gaia-x.eu/framework/#](https://docs.gaia-x.eu/framework/#)
       * Make sure to press the "Take the Tour"-button! :)


#### Gaia-X Hackathon No. 5



**Promotion**

   * Hackathon Teaser video available - **spread the word!**
   * Link: [https://youtu.be/fnVF8oCE-qc](https://youtu.be/fnVF8oCE-qc)


**Proposals**

   * Proposals have been collected: [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/Hackathon-5-Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/Hackathon-5-Proposals)
       * 01 MoveID - Integration of a Mobility Data Ecosystem into the Gaia-X Framework
       * 02 Privacy-preserving Gaia-X Edge to Cloud integration
       * 03 Self-Descriptions
       * 04 GXFS
       * 05 Gaia-X Trust Framework - Latest updates
       * 06 Self-Description Creation Wizard
       * 07 Cloud federation via OIDC
       * 08 Collection Modules of the Continuous Automated Monitoring (CAM) - Compliance
       * 09 EDC - Minimum Viable Data Spaces
       * 10 SSI-based Auth using Gaia-X SD \& OPA / Rego
   * GXFS Onboarding and Consulting will be a focus of the Hackathon with the release of the GXFS


**Hackathon No. 5 Agenda**

   * Agenda will be finalized in the next days, the current agenda is preliminary
   * Link: [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home)
   * **Your feedback is needed!** Everything good? Something missing? ...
       * New approach to create controlled vocab for SD [https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/issues/131](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/issues/131)
       * A project to follow regarding identity: [https://polygon.technology/polygon-id/](https://polygon.technology/polygon-id/)


**Call for organizers!** We need some helping hands - if you'd like to volunteer to support the organization of a track, please reach out and we'll add you to the organizer meetings. Your support is much appreciated!



**Don't forget to register for the Hackathon!** Registration is **mandatory** in order to receive access to the sessions.
