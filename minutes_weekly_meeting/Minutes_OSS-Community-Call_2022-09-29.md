
# Meeting notes Gaia-X OSS community September 29, 2022



## Focus of the weekly

   * Hackathon No. 5


## Proposed Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of Agenda

3. Introduction of new participants and regular contributors 

4. Interesting sessions, events and news

5. Organizational topics

6. Work on the Deliverables

7. AOB 



## General Notes

Participants in the call: 20

Agenda: Accepted



### Introduction of new participants and regular contributors

   * Walid Jaballi
   * Mike Lorusso
   * Tuikka Tuomo
   * Carlos Cuezva Tordable


### Interesting sessions, events and news

   * Gaia-X Summit 2022: [https://gaia-x.eu/event/gaia-x-summit-2022/](https://gaia-x.eu/event/gaia-x-summit-2022/)
   * General links for newcomers:
       * [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
       * [https://gitlab.com/gaia-x/data-infrastructure-federation-services](https://gitlab.com/gaia-x/data-infrastructure-federation-services)
       * [https://gitlab.com/gaia-x/lab/compliance](https://gitlab.com/gaia-x/lab/compliance)
       * [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu/)
   * Cristina's GitLab Life Hack: Click on the small symbol next to a headline and you can link directly to that section of the page!


### Work on the Deliverables

**Hackathon No. 5 Recap**

   * Result presentations: [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home#final-result-presentations](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home#final-result-presentations)
       * More presentations will be added
   * Rapid onboarding, faster than in the past, dedicated hacking day on day 2

