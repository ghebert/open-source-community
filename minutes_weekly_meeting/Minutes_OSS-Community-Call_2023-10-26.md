## Meeting notes Gaia-X OSS community October 26, 2023

### Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   6. AOB

###  Focus of the weekly

   * Updates from the community

### General Notes

   * **Participants in the call:** 47
   * **Acceptance of last week's minutes:** Yes
   * **Acceptance of the agenda:** Yes
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-10-19.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-10-19.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Christian Kahr
   * Maro Baka, Data Cellar


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**Confirmed! Pre-GaiaX Summit Open Source Meetup! organized by the Eclipse Foundation**

   * When: November 8 - 16:00-18:30 - followed by a cocktail
   * Where: Eurostars Centrum Alicante - Alicante (Spain)
   * Register at [https://pregaiaxsummitmeetup.eventbrite.com/](https://pregaiaxsummitmeetup.eventbrite.com/)
   * Still working on the program - available slots for short presentations (10mn) about OSS projects related to Gaia-X whether hosted at Eclipse or not. Send gael.blondelle@eclipse-foundation.org a presentation title and speaker name.


**Gaia-X Summit 2023**

   * November 9-10, Alicante (Spain)
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


**Gaia-X Workshop Luxembourg (only for Gaia-X Hubs, Ecosystems, Lighthouse projects)**

   * December 5-6 (maybe also 4) - Note, this is not an open event
   * Location: Luxembourg
   * Register: [https://events.gaia-x.lu/data-summit-luxembourg-schengen-x](https://events.gaia-x.lu/data-summit-luxembourg-schengen-x)
   * Info: [https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh](https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh)


**GXFS Tech Workshop**

   * December 12-13, Cologne (Germany)
   * Location: 25hours Hotel Koeln The Circle
   * Registration [https://www.gxfs.eu/gxfs-tech-workshop-5/](https://www.gxfs.eu/gxfs-tech-workshop-5/) 
   * Agenda coming 
   * On site event - no remote participation 




### Updates from other lighthouses / projects / developers / lab?

   * The Gaia-X Lab is wrapping up the work on Tagus, and it's expected to go into maintenance mode after the Summit
   * [https://gitlab.com/-/snippets/3612759](https://gitlab.com/-/snippets/3612759) Snippet on how to move to "production" Gaia-X credentials
 

### AOB

   * Discussion about how to be listed on the Gaia-X Clearing house page [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house):
       * - Reach out to frederik.tengg@gaia-x.eu to get the formal procedures of becoming a clearing house
       * - After that is complete, you will need to provide us the public endpoints where the mandatory components can be reached by users
       * - We will do some testing to validate that the new clearing house is working as expected, after which we'll add it to the clearing house page
   * Cristina is leaving Gaia-X at the end of October, you can reach out to her at @cristinapauna:matrix.org
