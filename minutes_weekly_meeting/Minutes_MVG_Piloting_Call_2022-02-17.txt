MVG/Piloting Call: 2022-02-17

1. Interesting sessions/events/news

Andreas Weiss: 
- How do we filter which events/news/... are interesting?
- Currently Gaia-X sessions with up to 1,000 participants
-> Showing what Gaia-X does and where we are right now
-> E.g. with Netherlands, Luxembourg
- Digital Responsisiblity Goals will be released next week
- What is of interest?

Marius Feldmann: 
- Whatever feels interesting to you, what is being done, what kind of networks are being generated,...

Andreas Weiss: 
- Event calendar for collecting interesting events would be helpful
-> Takes this as an action item

Lauresha Memeti:
- GXFS calendar: https://www.gxfs.eu/events/

Kurt Garloff: 
- Open Infrastructure summit in Berlin (June 2022)
-> Gaia-X community will be very present

2. Status of Gaia-X Hackathon #3 preparation

All Hackathon information and organization can be found here:
https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-3/-/wikis/GX-Hackathon-3

Focus of Hackathon #3:

- Discussions on whether Hackathon #3 should focus on making Gaia-X real and defining roadmap
-> Result: Roadmap is not being defined by MVG, but by AISBL and other entities such as GXFS and eco (as well as partners on the french side of Gaia-x)
- What is the focus of the Hackathon?
-> Creating connections and synchronizing different activities and projects
-> If you observe any issues or frustrations: Invite people who are working on Gaia-X to the Hackathon to clear up any misalignemnts

Basic Information:

- Date: 28th/29th of March 2022
- Hackathon coordination is done by the Gaia-X AISBL & MVG group with support from eco
- For questions, remarks and any other discussions feel free to use the MVG mailing list at any time!
- Details for registration will follow until end of February
- Hackathon #2 had a landing page
-> Kai Meinke will update it for Hackathon #3
-> Repository is open for everyone

Please note: Hackathon is not meant to be for product display
-> Technical contribution is required
-> In case you want to contribute: make sure there is real alignment

Track Updates:

Track 1: Newcomer & General Track (Marius Feldmann, Josephine Seifert):

Marius Feldmann:
- General overview of Gaia-X (approx. 45 minutes)
-> Based on official material by AISBL
-> Tutorial approach as well
-> Align with current state of Self Descriptions and GXFS
- Content is currently in progress, will be delivered latest in two weeks 

Track 2: Service & Tool Support Track (Martin Pilka & Kai Meinke):

Kai Meinke:
- Current focus on Self Descriptions
-> Lots of work on this in different groups
- SSI Integration
-> Issue credentials, verification, credential exchange,...
- Many working Open Source repositories for participants to get started

Andreas Weiss:
- Track 2: Focus more on tooling side
- Track 3: Focus more on content side

Martin Pilka:
- Goal is making some libraries available in Python 
-> Matej Feder is supporting 

Matej Feder:
- Development has started
- Currently looking which tools are really needed and which are already existing in the Self Descriptions WG
-> Communication on this also with Kurt Garloff (SSI community) and walt.id

Track 3: GXFS Track (Emma Wehrwein):

Andreas Weiss:
- Currently consolidating all project plans that were submitted by implementation parties
-> Till end of February
- Working closely together with Pierre Gronlier
-> Areas: Registry and Trust Framework
- Everything needs to be matched and aligned

Track 4: ACME Track (Kurt Garloff):

Kurt Garloff: 
- Many discussions and ideas
-> Now deciding on what to do
- One key aspect: In Hackathon #2 deployments of SCS were done live
-> In Hackathon #3: installations before the event to make sure infrastructure is running without issues
- Working on automation of SD and demonstrating their usefulness
- Big Data use case is currently being discussed

Marius Feldmann:
- Important: Provider perspective should be in focus
-> What does a provider have to do (technically) to be Gaia-X compliant?

Kurt Garloff:
- Working in this direction
- Challenge: Definition of compliance not finalized yet

Andreas Weiss:
- Please follow the evolution of the Gaia-X Trust Framework:  https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/

Kai Meinke: 
- Confidential Computing was added, might be a fit for Track 6 as well

Marius Feldmann:
- What is the story here?
-> Provider has Confidential Computing environment
-> Wants to show this in Gaia-X
-> Challenge: How can this be demonstrated in the SD? How can this attribute be added?

Markus Leberecht: 
- Currently working on this process for non-mandatory attributes for CC
-> Similar as Bare Metal as a Service Tutorial
--> Use this as template and extend towards CC

Marius Feldmann: 
- Role of Hackathon is not CC, but how such information can be brought into the Gaia-X ecosystem
- CC is a really good use case for how the Gaia-X tooling can be challenged

Christoph Lange-Bever: 
- Confirms that process is how to define further attributes of Gaia-X wide relevance
- Process for when schemas are lacking attributes exists (Governance Process)
-> Not limited to mandatory attributes but also for non-mandatory attributes
-> Please see the following link for the proces for suggesting attributes for the SD schemas:  https://gitlab.com/gaia-x/gaia-x-community/gaia-x-self-descriptions/-/tree/master/documentation/governance-process

Track 5: Eclipse Dataspace Connector Track (Markus Spiekermann):

Marius Feldmann:
- Marius will request details from Markus Spiekermann and Stefan Ettl
- Similar to Hackathon #2
- No further updates today

Track 6: Deployment / Minimal Viable Gaia-X Track (Kai Meinke, Matej Feder):

Marius Feldmann:
- Extension of pilot-004: deployment perspective
-> Someone would like to deploy an application on the Gaia-X infrastructure
-> Similar questions as in Track 4

Kai Meinke:
- Focus on SD and making infrastructure transparent in a Gaia-X compliant way
- How can Catalogue be populated, how does it look in Gaia-X compliant way
- SSI direction: How to interact with wallet, how to onboard and get accredited as Gaia-X member
- Extend MVG as an MVP towards GXFS

Matej Feder: 
- Connected with MVG pilot, introduce pilot-005 as extension of pilot-004
- Integrate Gaia-X Catalogue with pilot-005 architecture
- Use things from Hackathon #2 (open tasks regarding SSI Integration)

Geert Machtelinckx:
- Not sure where to position his project (operationalization & interoperability)
- Everything is already very defined based on existing tooling

Kai Meinke:
- Please outline proposal via mailing list
- We don't need the perfect place right now
- Still plenty of space in the agenda
- Talk to track organizers, share ideas with community
- We're always looking for contributions for Hackathon and especially responsible people who will be present during the Hackathon 

Marius Feldmann: 
- Important: Don't come up with a full proposal on what Gaia-X could look like etc
- We need very clear technical relation to what we have in focus (e.g. GXFS)
- What can we do, from user perspective or provider perspective (compliance)
- Can you deliver a technical contribution to help with that?

3. AOB

[nothing]