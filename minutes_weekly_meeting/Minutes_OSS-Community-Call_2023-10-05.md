## Meeting notes Gaia-X OSS community October 05, 2023

### Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Update on the Gaia-X Framework / GXDCH
   6. Updates from other lighthouses / projects / Gaia-X Lab/ developers 
   7. AOB

###  Focus of the weekly

   * Updates from the community


### General Notes

   * **Participants in the call:** 39
   * **Acceptance of Agenda:** Yes
   * **Acceptance of last week's minutes:** Yes
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-09-28.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-09-28.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * No new participants in the call.


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**Gaia-X Roadshow** 

   * October 10, 6 - 11pm, Wolfsburg (Germany)
   * Registration: [https://www.eco.de/event/gaia-x-roadshow-wolfsburg](https://www.eco.de/event/gaia-x-roadshow-wolfsburg) 


**EuProGigant Open House Day**

   * October  10 - 11, Berlin (Germany)
   * EuProGigant ecosystem of five projects
   * Registration open, 250 PAX, physical participation only
   * [https://euprogigant-openhouseday2023.b2match.io/](https://euprogigant-openhouseday2023.b2match.io/)


**Future Congress Digital** 

   * October 11, Wolfsburg (Germany)
   * Gaia-X and Digital Mobility Ecosystems
   * [https://www.futurecongress.digital/anmeldung](https://www.futurecongress.digital/anmeldung)


**EclipseCon**

   * October 16 - 19, Ludwigsburg (Germany)
   * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
   * Workshop "**Gaia-X Federation Services @ Eclipse**" during the community day on Monday, October 16th 
       * [https://www.eclipsecon.org/2023/gaia-x-federation-services-eclipse](https://www.eclipsecon.org/2023/gaia-x-federation-services-eclipse)


**GXFS Tech Workshop**

   * October 17, Reutlingen (Germany)
   * Registration and Agenda [https://www.eco.de/event/gxfs-tech-workshop-4/](https://www.eco.de/event/gxfs-tech-workshop-4/) 
   * Location: **INNOPORT Reutlingen** Max-Planck-Straße 68/1/ 72766 Reutlingen
   * Topics:
       * _„Was tragen Initiativen wie Gaia-X und Manufacturing-X zur Digitalisierung der Industrie bei?"_ - In German
       * Hands on with the WFE - In English 

**SCS Hackathon**

   * Nov 8, Dresden (Germany)
   * More info soon!

**Gaia-X Summit 2023**

   * November 9-10, Alicante (Spain)
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


**GXFS Tech Workshop**

   * December 12-13, Cologne (Germany)
   * Location: 25hours Hotel Koeln The Circle
   * Registration [https://www.gxfs.eu/gxfs-tech-workshop-5/](https://www.gxfs.eu/gxfs-tech-workshop-5/) 
   * Agenda coming 

### Update on the Gaia-X Framework / GXDCH

   * GXDCH Status and overview has been added
       * [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)


### Updates from other lighthouses / projects / developers / lab?

   * Pontus-X custom catalogue filter categories integration upcoming (short demo)
   * Pontus-X Browser Auto-Signer integration upcoming with State Library of Berlin short demo
       * [https://sbb.pontus-x.eu/](https://sbb.pontus-x.eu/)
       * Gaia-X compliant service credentials for services will be published next
   * Quick recap on the Credential Event Service (CES) for the creation of the federated catalogue
       * [https://portal.moveid.eu/asset/did:op:b4be1f60c197890bfebc041e66731a2c6f98f520169a9f38173c19e29353b4d0](https://portal.moveid.eu/asset/did:op:b4be1f60c197890bfebc041e66731a2c6f98f520169a9f38173c19e29353b4d0)
       * [https://portal.moveid.eu/verify?did=did%3Aop%3Ab4be1f60c197890bfebc041e66731a2c6f98f520169a9f38173c19e29353b4d0](https://portal.moveid.eu/verify?did=did%3Aop%3Ab4be1f60c197890bfebc041e66731a2c6f98f520169a9f38173c19e29353b4d0)
       * [https://compliance.gaia-x.eu/v1/docs](https://compliance.gaia-x.eu/v1/docs)
       * [https://ces-v1.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/post\_credentials\_events](https://ces-v1.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/post\_credentials\_events)
       * c20b53b3-d994-4529-bf22-354269765a66
   * XFSC PCM/OCM integration in Service-Meister / Pontus-X ongoing to increase cross-ecosystem interoperability for Gaia-X projects.
   * LAB: Ewann \& Arthur still working on Resources (DataResource, SoftwareResource,PhysicalResource, ServiceAccessPoint,InstantiatedVirtualResource), both shapes \& compliance credential for them. Expecting to deliver on development this or next week for test purpose. Probably not perfect, but still a step forward :)
 

### AOB

   * Nothing



