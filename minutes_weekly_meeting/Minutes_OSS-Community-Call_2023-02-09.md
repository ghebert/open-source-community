# Meeting notes Gaia-X OSS community February 9, 2023

## Focus of the weekly

   * Self-Description Hacking Event with acatech / Gaia-X Hub Germany
   * Hackathon No. 6


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Tech event \& Hackathon #6

6. AOB



## General Notes

**Participants in the call:** 27

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-02-02.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-02-02.md)

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Nils Weiher
   * Carsten Stocker


**Useful resources for newcomers:**

   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)


### Interesting sessions, events and news

   * **Gaia-X Roadshow Hamburg**
       * February 9, Hamburg
       * Organized by eco
       * [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)
       * [https://www.eco.de/event/gaia-x-roadshow-powered-by-eco-hamburg/#tickets](https://www.eco.de/event/gaia-x-roadshow-powered-by-eco-hamburg/#tickets)
       * Restaurant Vlet in der Speicherstadt


   * **FOSS Backstage 23**
       * March 14-15, Berlin (and online)
       * Theme: *Community, Management \& Compliance in Open Source Development*
       * [https://23.foss-backstage.de/](https://23.foss-backstage.de/)
       * Eduard \& Ariane will be there to present **Community2**
           * Community2 is our new community for Open Source Community Managers
           * More information: [https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop](https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop)


   * **Market-X, Vienna, Gaia-X Market Adoption Event**
       * March 14-15, Vienna, Aula der Wissenschaften, [https://www.aula-wien.at/](https://www.aula-wien.at/) 
       * Updated website: [https://gaia-x.eu/market-x/](https://gaia-x.eu/market-x/)
       * Market-X is a great opportunity to present projects, find business matches, connect with our lighthouse projects and other initiatives, start building cross-industry collaborations, learn about Gaia-X value chains on different levels and become part of Gaia-X market adoption community.
       * The event will feature a large exhibition where you can network and connect with the community, as well as different booth options to showcase hubs, projects, verticals, and get visibility within the Gaia-X community. Learn about the booth options at  [https://gaia-x.eu/market-x/](https://gaia-x.eu/market-x/)
       * 

   * New event series: GXFS Tech Workshops Key contact vivien.witt@eco.de
   * Please save the date for **15 and 16 March 2023**. The Identity \& Trust Workshop and the Self-Description Workshop will take place on these consecutive days. Location tbd


           * Before planning your trip, please note that knowledge in the following technical areas and access to specific tools are **[]mandatory[]**. If you do not have experience with these tools, a participation in the workshops will be difficult: 
               * Postman,     Curl, Docker, Kubernetes, Github Access necessary
               * Javascript, Java, Go or any     other programming language
               * Development     IDE on the notebook (e.g. Visual Studio Code)
               * Gitlab,     Minikube, k3s or kubernetes cluster are good to have 
               * IOS/Android Smartphone,     Downloaded PCM App (we will be sharing instructions for the PCM download     ahead of the workshop) 
           * Livestream also planned for passive participation


   * **Data Spaces Symposium \& Deep-Dive Day**
       * March 21–23, The Hague
       * Info and registration link: [https://internationaldataspaces.org/data-spaces-symposium]([]https://internationaldataspaces.org/data-spaces-symposium[])


   * **Hannover Messe, Hannover, Germany**
       * April 17-21, Hanover (five days is new in 23)
       * [https://www.hannovermesse.de/en/](https://www.hannovermesse.de/en/)
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de
       * Gaia-X Area


   * **Cloud Expo Europe, Frankfurt, Germany**
       * May 10-11, Frankfurt
       * [https://www.cloudexpoeurope.de/themes2023](https://www.cloudexpoeurope.de/themes2023)
       * Gaia-X at the heart of the Expo, Gaia-X Arena 
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de


   * **MyData conference**
       * May 31 - June 01, Helsinki
       * [https://www.mydata.org/event/mydata-2023/](https://www.mydata.org/event/mydata-2023/)


   * **Tech-X \& Hackathon #6**
       * 3\&4th of May, Bilbao, Spain
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home)
   * 



### Self-Description Hacking Event

- Last Friday, February 3, organized by the German Gaia-X Hub and acatech with deltaDAO

- More than 75 institutions received Self-Descriptions

- Access to Pontus-X - Gaia-X Web3 ecosystem

- Participant Examples: [https://docs.genx.minimal-gaia-x.eu/docs/Community/participants](https://docs.genx.minimal-gaia-x.eu/docs/Community/participants) 



### GXFS Workflow Engine

- Andreas presenting the current work on the workflow engine based on ([https://nodered.org/)](https://nodered.org/))

- Example: Validation with the Compliance Service from creation to verification

- Working on Catena-X Onboarding Examples for HMI Hannover Messe



Examples for REGO Policies

- A collection of examples is appreciated and useful for reuse in the developer community.

- [https://gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/policies](https://gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/policies) TSA examples 



### Tech event \& Hackathon No. 6

   * **Infos and Guideline**
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home)


   * **Location and date**
       * Azkuna Zentroa in Bilbao, Spain on the 3\&4th of May 2023


   * **Tech-X on site event**
       * The presentations and workshops will be on-site
       * For the Hackathon:
           * The event will by default not be hybrid, but Session Organizers can have people help remotely if they can organize it efficiently for their individual sessions
           * If you are contributing a session you should have at least one person on site to represent the team
           * There will be some prizes and ... there will be Gaia-X Hackathon T-Shirts again for the hacking contributors on-site
       * After the proposal phase end there will be about two weeks to decide on participation in the hacking sessions \& workshops to allow for planning and collaboration before the event.


   * **Registration**
       * Will be open in a few weeks


   * **Call for proposals**
       * If you have anything in mind for the Hackathon and/or the tech event, please add to the proposals page (or align on suggestions that have already been added, preferably via the mailing list) [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals)
       * Deadline for proposals: March 31, 2023


   * **Call for organizers**
       * If you would like to help organizing the Hackathon, please reach out on the mailing list or to Cristina!






### AOB

   * Nothing.
