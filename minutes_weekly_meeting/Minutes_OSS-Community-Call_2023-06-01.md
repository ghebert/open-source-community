# Meeting notes Gaia-X OSS community June 1, 2023

## Agenda

   1. Competition \& Antitrust guidelines
   1. Acceptance of last week meeting notes and today's agenda
   1. Introduction of new participants and regular contributors
   1. Interesting sessions, events and news
   1. SCS Summit Report
   1. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   1. AOB


##  Focus of the weekly

   * Updates from the community
   * SCS summit report

## General Notes

**Participants in the call:** 23

**Acceptance of Agenda:** Accepted by the audience.

**Acceptance of last week's minutes:** Accepted by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-05-25.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-05-25.md)

**Link to all minutes:** https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes_weekly_meeting


### Introduction of new participants and regular contributors

   * George Sidiras 

**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Compliance Service Swagger UI [https://compliance.lab.gaia-x.eu/v1/docs/](https://compliance.lab.gaia-x.eu/v1/docs/)
   * Gaia-X Registry Swagger UI: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Gaia-X Registry: [https://registry.lab.gaia-x.eu/](https://registry.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news


   * **Hackathon #6 Report is available!**
       * Link: [https://gaia-x.eu/news/latest-news/gaia-xs-hackathon-6-concludes/](https://gaia-x.eu/news/latest-news/gaia-xs-hackathon-6-concludes/)


   * **Gaia-X General Assembly 2023**
       * June 5, 2023
       * [https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/11](https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/11)
       * New Board of Directors (24+2) will be elected by members


   * **Webinar DG Grow on Standardisation Digital Product Pass June 12th**

       * In order to present the draft standardisation request to a wide group of potentially interested stakeholders, we are organising a Webex webinar next **12 June 2023, from 14:00 until 17:00 CET**. The webinar will also give us the possibility to update stakeholders on the current status of negotiation in the Council and Parliament with regards to the DPP, to provide elements of reflection related to possible DPP design options, updates from the EU CIRPASS project, and future funding opportunities related to DPP testing.


       * Webinar []draft[] agenda:
 

       * **14:00 – 14:10**: Welcome and housekeeping info (Michele Galatola, DG GROW)
       * **14:10 – 14:30**: Update on the co-decision process (William Neale, DG ENV)
       * **14:30 – 15:00**: The draft standardisation request on DPP system (Michele Galatola, DG GROW)
       * **15:00 – 15:30**: Q\&A from stakeholders
       * **15:30 – 15:45**: Break
       * **15:45 – 16:15**: First considerations on possible technical solutions for the design of the DPP system (Joao Rodrigues Frade, DG DIGIT)
       * **16:15 – 16:35**: The EU CIRPASS project and its contribution to DPP system design (Carolynn Bernier, CIRPASS project coordinator)
       * **16:35 – 16:45**: Upcoming Digital Europe funding opportunities related to DPP (Ilias Iakovidis, DG CNECT)
       * **16:45 – 16:55**: Q\&A
       * **16:55 – 17:00**: Conclusions and next steps (Michele Galatola, DG GROW)
       *  
       * If you want to attend this webinar please use this link: [https://ecconf.webex.com/ecconf/j.php?MTID=meda817498ac42b65479e72db1a31f23f](https://ecconf.webex.com/ecconf/j.php?MTID=meda817498ac42b65479e72db1a31f23f)


   * **GXFS Tech Workshop**
       * 21 \& 22 June in Braunschweig - Physical only
       * Register here: [https://www.eco.de/event/gxfs-tech-workshop-2/](https://www.eco.de/event/gxfs-tech-workshop-2/) 
       * The workshop will be comprised of introductory sessions and exercises on day one focusing on the GXFS work packages identity and trust as well as catalogue and asset descriptions. We will also hear the latest update on EDC components. On day two, we will have hacking sessions focused on exploring how GXFS and EDC components can be used.


   * **Eclipse XFSC Into webinar**
   * [https://www.linkedin.com/posts/emma-wehrwein-eco\_gxfs-xfsc-xfsc-activity-7069670712877150210-5VA3?utm\_source=share\&utm\_medium=member\_desktop](https://www.linkedin.com/posts/emma-wehrwein-eco\_gxfs-xfsc-xfsc-activity-7069670712877150210-5VA3?utm\_source=share\&utm\_medium=member\_desktop)
   * 

   * **New GXFS Whitepaper** [https://www.gxfs.eu/kritis-whitepaper/](https://www.gxfs.eu/kritis-whitepaper/)
       * Which rules operators of critical infrastructures have to follow in the context of Gaia-X, is discussed in the new Gaia-X Federation Services (GXFS-DE) whitepaper “Information Security of Critical Infrastructures – Law and Regulation for Gaia-X and the Gaia-X Federation Services”.


   * **5th HACKATHONAMRING - Physical only**
       * Date and time: June 23 · 4pm - June 25 · 4pm CEST
       * [https://www.eventbrite.de/e/5th-hackathonamring-tickets-190977106667?aff=ebdssbdestsearch](https://www.eventbrite.de/e/5th-hackathonamring-tickets-190977106667?aff=ebdssbdestsearch) 

   * **EclipseCon, Ludwigsburg, Germany**
       * October 16 - 19, 2023
       * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
       * Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects
       * CFP is currently open until June 16. Early submission deadline is June 2 to have a better chance to have your proposition accepted.


### SCS Summit Report (Kurt Garloff)

   * (Written report, sorry!)
   * 140+ registered participants (~15% no-show rate) in Berlin BBAW May 23+24, 2023
       * Mostly German audience, most sessions in German
       * Participants: Politics (member of German Parliament, German Ministry for Economy and Climate Action, Agency for disruptive innovation, Ministry for Economic Cooperation and Development, ...), Companies (C-level from PlusServer, OSISM, dataport, DATEV, ...), Civil Society (VOICE, Gov.Digital, eco/GXFS, ...) ...
   * Digital Sovereignty was THE topic - from the need and concepts to practical implementation
       * Message: With SCS and GXFS, we now have the software that helps us to build infrastructure in support of digital sovereignty -- and we have partners that already implement it in production (OSISM, PlusServer, Noris/Wavecon, Regio.digital, AOV, ...) and more are upcoming (e.g. Osnabrück University, DC of the state of Thuringia, ...)
   * Program at [https://scs.community/summit/](https://scs.community/summit/), Slides and Recordings are collected there (work-in-progress) as well


### Updates from other lighthouses / projects / developers?

   * LAB: Rewriting of the ontology using Protégé (based on work done by Roberto Garcia) [https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/merge\_requests/177](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/merge\_requests/177)  - Breaking changes expected as we will generate schemas and shapes based on this ontology instead of manual shape development
   * Discussion around data processing in a TEE, how we move forward on running dataspace on TEE, prevent data leaking out etc. Markus Leberecht proposed a small video to present a DS running in TEE: [https://youtu.be/c9vtlT7OCPs](https://youtu.be/c9vtlT7OCPs)
   * Decentralised EDC with GX-Compliant DID:WEB is now possible, smartSense Solutions has prepared a POC for the same. Here are the open-source repositories for a simple POC
       * [https://github.com/smartSenseSolutions/decentralized-edc-connector](https://github.com/smartSenseSolutions/decentralized-edc-connector)
       * [https://github.com/smartSenseSolutions/edc-frontend](https://github.com/smartSenseSolutions/edc-frontend)
       * [https://github.com/smartSenseSolutions/decentralized-edc-connector](https://github.com/smartSenseSolutions/decentralized-edc-connector)


### AOB

#### **Task for the Community**

(not discussed in this meeting) 

**Express your expectations to the OSS-Community meeting (via mailing list, Slack/Matrix, here in the pad)**

   * What information are you looking for?
   * What do you expect?
   * How can we make this more useful?
